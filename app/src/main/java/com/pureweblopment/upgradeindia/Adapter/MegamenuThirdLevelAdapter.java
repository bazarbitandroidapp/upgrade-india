package com.pureweblopment.upgradeindia.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pureweblopment.upgradeindia.Global.Global;
import com.pureweblopment.upgradeindia.Global.SendMail;
import com.pureweblopment.upgradeindia.Global.SharedPreference;
import com.pureweblopment.upgradeindia.Global.StaticUtility;
import com.pureweblopment.upgradeindia.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class MegamenuThirdLevelAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    Fragment mFragment;
    String[] subChildName;
    String[] subChildSlug;
    String[] subChildImage;
    ThirdLevelClick mThirdLevelClick;

    public MegamenuThirdLevelAdapter(Context mContext, Fragment mFragment, String[] subChildName, String[] subChildSlug, String[] subChildImage) {
        this.mContext = mContext;
        this.mFragment = mFragment;
        this.subChildName = subChildName;
        this.subChildSlug = subChildSlug;
        this.subChildImage = subChildImage;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_third_level, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int i) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.mTxtSubCategory.setText(subChildName[i]);

            //region Image
            String picUrl = null;
            try {
                URL urla = null;
                /*images = images.replace("[", "");
                image = image.replace("]", "");*/
                urla = new URL(subChildImage[i].replaceAll("%20", " "));
                URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                picUrl = String.valueOf(urin.toURL());
                // Capture position and set to the ImageView
                Picasso.get()
                        .load(picUrl)
                        .into(viewHolder.mImgSubCategory, new Callback() {
                            @Override
                            public void onSuccess() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onError(Exception e) {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }
                        });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(mContext, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(mContext, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            }
            //endregion

            viewHolder.mLlSubChildCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mThirdLevelClick = (ThirdLevelClick) mFragment;
                    mThirdLevelClick.selectedSubCategory(subChildName[i], subChildSlug[i]);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return subChildName.length;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImgSubCategory;
        private TextView mTxtSubCategory;
        private LinearLayout mLlSubChildCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            mImgSubCategory = itemView.findViewById(R.id.imgSubCategory);
            mTxtSubCategory = itemView.findViewById(R.id.txtSubCategory);
            mLlSubChildCategory = itemView.findViewById(R.id.llSubChildCategory);
            if (SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
                mTxtSubCategory.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            }
        }
    }

    public interface ThirdLevelClick{
        void selectedSubCategory(String Value, String Slug);
    }
}
