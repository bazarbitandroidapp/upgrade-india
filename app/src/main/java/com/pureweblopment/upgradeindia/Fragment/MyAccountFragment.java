package com.pureweblopment.upgradeindia.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.pureweblopment.upgradeindia.Activity.CurrencyScreen;
import com.pureweblopment.upgradeindia.Activity.LoginActivity;
import com.pureweblopment.upgradeindia.Activity.MainActivity;
import com.pureweblopment.upgradeindia.Activity.CommissionScreen;
import com.pureweblopment.upgradeindia.Global.Global;
import com.pureweblopment.upgradeindia.Global.SharedPreference;
import com.pureweblopment.upgradeindia.Global.Typefaces;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.concurrent.Executors;

import com.pureweblopment.upgradeindia.Global.SendMail;
import com.pureweblopment.upgradeindia.Global.StaticUtility;

import com.pureweblopment.upgradeindia.R;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyAccountFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyAccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyAccountFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ImageView imageCartBack, imageNavigation, imageLogo;
    FrameLayout frameLayoutCart;
    TextView txtCatName;
    CardView cardviewBottomNavigation;

    private OnFragmentInteractionListener mListener;

    CircleImageView circularImageViewUser;
    /*CircularImageView circularImageViewUser;*/
    TextView txtUserName, txtUserEmail, txtUserReferralCode, txtwallet;
    ImageView imageMyAccount, imageMyAddress, imageOrderHistory, imageChangePWD,
            imageLogout, imageReviews, imagewallet;
    TextView txtMyAccount, txtMyAddress, txtOrderHistory, txtChangePWD, txtLogout,
            txtReviews, mTxtCurrency, mTxtCommission;
    LinearLayout llMyAccount, llMyAddress, llOrderHistory, llChangePWD, llLogout, llReviews,
            llMyAccountHeader, mLlCurrency, mLlCommission, llwallet;

    String picUrl = null, mbingage_wallet = "", mCurrency_Code = "";
    private static URL urla = null;
    private static URI urin = null;

    ImageView imgUser;

    RelativeLayout relativeProgress;

    public MyAccountFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyAccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyAccountFragment newInstance(String param1, String param2) {
        MyAccountFragment fragment = new MyAccountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainActivity.manageBackPress(false);
        View view = inflater.inflate(R.layout.fragment_my_account, container, false);


        imageCartBack = getActivity().findViewById(R.id.imageCartBack);
        imageNavigation = getActivity().findViewById(R.id.imageNavigation);
        imageLogo = getActivity().findViewById(R.id.imageLogo);

        frameLayoutCart = getActivity().findViewById(R.id.frameLayoutCart);
        txtCatName = getActivity().findViewById(R.id.txtCatName);
        cardviewBottomNavigation = getActivity().findViewById(R.id.cardviewBottomNavigation);

        imageCartBack.setVisibility(View.GONE);
        txtCatName.setVisibility(View.VISIBLE);
        cardviewBottomNavigation.setVisibility(View.VISIBLE);
        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        imageNavigation.setVisibility(View.VISIBLE);
        imageLogo.setVisibility(View.GONE);
        frameLayoutCart.setVisibility(View.GONE);

       /* String userID = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        if (userID != null) {

        } else {
            Toast.makeText(getContext(), "You have to Login First!", Toast.LENGTH_SHORT).show();
            SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
            SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
            SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
            SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
            SharedPreference.ClearPreference(getContext(), Global.ISCheck);
            startActivity(intent);
            getActivity().finish();
        }*/


        Initialization(view);
        TypeFace();
        OnClickListener();

        JSONArray jsonArray = null;
        mbingage_wallet = "0";
        try {
            jsonArray = new JSONArray(SharedPreference.GetPreference(getActivity(),
                    Global.APPSetting_PREFERENCE, StaticUtility.Feature_List));
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    if (jsonObject.optString("slug").equalsIgnoreCase("bingage-wallet")) {
                        if (jsonObject.optString("feature_type").equalsIgnoreCase("multiple")) {
                            mbingage_wallet = jsonObject.optString("feature_multiple");
                        } else if (jsonObject.optString("feature_type").equalsIgnoreCase("text")) {
                            mbingage_wallet = jsonObject.optString("feature_textval");
                        } else if (jsonObject.optString("feature_type").equalsIgnoreCase("single")) {
                            mbingage_wallet = jsonObject.optString("feature_checkval");
                        } else if (jsonObject.optString("feature_type").equalsIgnoreCase("checkbox")) {
                            mbingage_wallet = jsonObject.optString("feature_checkval");
                        } else if (jsonObject.optString("feature_type").equalsIgnoreCase("link")) {
                            mbingage_wallet = jsonObject.optString("feature_textval");
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mCurrency_Code = SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                StaticUtility.sCurrencyCode);
        if(mbingage_wallet.equalsIgnoreCase("1") && mCurrency_Code.equalsIgnoreCase("INR")){
            llwallet.setVisibility(View.VISIBLE);
            getbingageMemberInfo();
        }else {
            llwallet.setVisibility(View.GONE);
        }
        AppSettings();
        setDynamicString();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    private void setData() {
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_EMAIL) != null) {
            txtUserEmail.setText(SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE,
                    Global.USER_EMAIL));
        } else {
            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sGUEST_EMAIL_ADDRESS) != null) {
                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sGUEST_EMAIL_ADDRESS).equals("")) {
                    txtUserEmail.setText(SharedPreference.GetPreference(getContext(),
                            StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sGUEST_EMAIL_ADDRESS));
                } else {
                    txtUserEmail.setText(getString(R.string.guest_email_address));
                }
            } else {
                txtUserEmail.setText(getString(R.string.guest_email_address));
            }
        }
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Name) != null) {
            txtUserName.setText(SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Name));
        } else {
            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sGUEST_USER) != null) {
                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sGUEST_USER).equals("")) {
                    txtUserName.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sGUEST_USER));
                } else {
                    txtUserName.setText(getString(R.string.guest_user));
                }
            } else {
                txtUserName.setText(getString(R.string.guest_user));
            }
        }
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                StaticUtility.REF_CODE_ENABLED) != null) {
            if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE,
                    StaticUtility.REF_CODE_ENABLED).equalsIgnoreCase("0")) {
                mLlCommission.setVisibility(View.GONE);
                txtUserReferralCode.setVisibility(View.GONE);
            } else {
                mLlCommission.setVisibility(View.VISIBLE);
                if (SharedPreference.GetPreference(getContext(), StaticUtility.PREFERENCEREFERRALCODE, StaticUtility.sREFERRALCODE) != null) {
                    txtUserReferralCode.setVisibility(View.VISIBLE);
                    txtUserReferralCode.setText(SharedPreference.GetPreference(getContext(), StaticUtility.PREFERENCEREFERRALCODE, StaticUtility.sREFERRALCODE));
                } else {
                    txtUserReferralCode.setVisibility(View.GONE);
                }
            }
        } else {
            mLlCommission.setVisibility(View.GONE);
            txtUserReferralCode.setVisibility(View.GONE);
        }

        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.IS_MULTI_CURRENCY_ACTIVE) != null) {
            if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.IS_MULTI_CURRENCY_ACTIVE).equalsIgnoreCase("1")) {
                if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencyName) != null) {
                    mTxtCurrency.setText(SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencyName));
                } else {
                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCURRENCY) != null) {
                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCURRENCY).equals("")) {
                            mTxtCurrency.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCURRENCY));
                        } else {
                            mTxtCurrency.setText(getString(R.string.currency));
                        }
                    } else {
                        mTxtCurrency.setText(getString(R.string.currency));
                    }
                }
            } else {
                mLlCurrency.setVisibility(View.GONE);
            }
        } else {
            mLlCurrency.setVisibility(View.GONE);
        }

        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Profile_Picture) != null) {
            String stringProfile = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Profile_Picture);
            if (!stringProfile.equalsIgnoreCase("")) {
                imgUser.setVisibility(View.GONE);
                circularImageViewUser.setVisibility(View.VISIBLE);
                try {
                    picUrl = String.valueOf(SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE,
                            Global.USER_Profile_Picture));
                    urla = new URL(picUrl);
                    urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(),
                            urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    Picasso.get()
                            .load(picUrl)
                            .into(circularImageViewUser, new Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }

                                @Override
                                public void onError(Exception e) {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT,
                            "Getting error in MyAccountFragment.java When parsing image url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
            } else {
                imgUser.setVisibility(View.VISIBLE);
                circularImageViewUser.setVisibility(View.GONE);
            }
        } else {
            imgUser.setVisibility(View.VISIBLE);
            circularImageViewUser.setVisibility(View.GONE);
        }
    }

    private void setDynamicString() {
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ACCOUNT) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ACCOUNT).equals("")) {
                txtCatName.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ACCOUNT));
            } else {
                txtCatName.setText(R.string.myaccount);
            }
        } else {
            txtCatName.setText(R.string.myaccount);
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ACCOUNT) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ACCOUNT).equals("")) {
                txtMyAccount.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ACCOUNT));
            } else {
                txtMyAccount.setText(R.string.myaccount);
            }
        } else {
            txtMyAccount.setText(R.string.myaccount);
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHANGE_PASSWORD) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHANGE_PASSWORD).equals("")) {
                txtChangePWD.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHANGE_PASSWORD));
            } else {
                txtChangePWD.setText(R.string.changepwd);
            }
        } else {
            txtChangePWD.setText(R.string.changepwd);
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_HISTORY) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_HISTORY).equals("")) {
                txtOrderHistory.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_HISTORY));
            } else {
                txtOrderHistory.setText(R.string.orderhistory);
            }
        } else {
            txtOrderHistory.setText(R.string.orderhistory);
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ADDRESS) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ADDRESS).equals("")) {
                txtMyAddress.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ADDRESS));
            } else {
                txtMyAddress.setText(R.string.myaddress);
            }
        } else {
            txtMyAddress.setText(R.string.myaddress);
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREVIEW) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREVIEW).equals("")) {
                txtReviews.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREVIEW));
            } else {
                txtReviews.setText(R.string.reviews);
            }
        } else {
            txtReviews.setText(R.string.reviews);
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOMMISSION) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOMMISSION).equals("")) {
                mTxtCommission.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOMMISSION));
            } else {
                mTxtCommission.setText(R.string.commission);
            }
        } else {
            mTxtCommission.setText(R.string.commission);
        }
        if(SharedPreference.GetPreference(getActivity(), Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null &&
                !SharedPreference.GetPreference(getActivity(), Global.LOGIN_PREFERENCE, Global.USERTOKEN).
                        equalsIgnoreCase("")){
            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGOUT) != null) {
                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGOUT).equals("")) {
                    txtLogout.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGOUT));
                } else {
                    txtLogout.setText(R.string.logout);
                }
            } else {
                txtLogout.setText(R.string.logout);
            }
        }else {
            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sLOG_IN) != null) {
                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sLOG_IN).equals("")) {
                    txtLogout.setText(SharedPreference.GetPreference(getContext(),
                            StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOG_IN));
                } else {
                    txtLogout.setText(R.string.login);
                }
            } else {
                txtLogout.setText(R.string.login);
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE,
                StaticUtility.wallet_balance) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.wallet_balance).equals("")) {
                txtwallet.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.wallet_balance));
            } else {
                txtwallet.setText(R.string.redeem_from_wallet);
            }
        } else {
            txtwallet.setText(R.string.redeem_from_wallet);
        }
    }

    //region Initialization...
    private void Initialization(View view) {
        circularImageViewUser = view.findViewById(R.id.circularImageViewUser);
        imgUser = view.findViewById(R.id.imgUser);
        txtUserName = view.findViewById(R.id.txtUserName);
        txtUserEmail = view.findViewById(R.id.txtUserEmail);
        txtUserReferralCode = view.findViewById(R.id.txtUserReferralCode);

        txtMyAccount = view.findViewById(R.id.txtMyAccount);
        txtMyAddress = view.findViewById(R.id.txtMyAddress);
        txtOrderHistory = view.findViewById(R.id.txtOrderHistory);
        txtChangePWD = view.findViewById(R.id.txtChangePWD);
        txtLogout = view.findViewById(R.id.txtLogout);
        txtReviews = view.findViewById(R.id.txtReviews);
        mTxtCurrency = view.findViewById(R.id.txtCurrency);
        mTxtCommission = view.findViewById(R.id.txtCommission);
        txtwallet = view.findViewById(R.id.txtwallet);

        llMyAccount = view.findViewById(R.id.llMyAccount);
        llMyAddress = view.findViewById(R.id.llMyAddress);
        llOrderHistory = view.findViewById(R.id.llOrderHistory);
        llChangePWD = view.findViewById(R.id.llChangePWD);
        llLogout = view.findViewById(R.id.llLogout);
        llReviews = view.findViewById(R.id.llReviews);
        llMyAccountHeader = view.findViewById(R.id.llMyAccountHeader);
        mLlCurrency = view.findViewById(R.id.llCurrency);
        mLlCommission = view.findViewById(R.id.llCommission);
        llwallet = view.findViewById(R.id.llwallet);

        imageMyAccount = view.findViewById(R.id.imageMyAccount);
        imageMyAddress = view.findViewById(R.id.imageMyAddress);
        imageOrderHistory = view.findViewById(R.id.imageOrderHistory);
        imageChangePWD = view.findViewById(R.id.imageChangePWD);
        imageLogout = view.findViewById(R.id.imageLogout);
        imageReviews = view.findViewById(R.id.imageReviews);
        imagewallet = view.findViewById(R.id.imagewallet);

        relativeProgress = view.findViewById(R.id.relativeProgress);
    }
    //endregion

    //region TypeFace...
    private void TypeFace() {
        txtUserEmail.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtUserName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtUserReferralCode.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtMyAccount.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtMyAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtOrderHistory.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtChangePWD.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtLogout.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtLogout.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtReviews.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        mTxtCurrency.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        mTxtCommission.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtwallet.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
    }
    //endregion

    //region OnClickListener...
    private void OnClickListener() {
        llMyAddress.setOnClickListener(this);
        llMyAccount.setOnClickListener(this);
        llOrderHistory.setOnClickListener(this);
        llChangePWD.setOnClickListener(this);
        llLogout.setOnClickListener(this);
        llReviews.setOnClickListener(this);
        mLlCurrency.setOnClickListener(this);
        mLlCommission.setOnClickListener(this);
        llwallet.setOnClickListener(this);
    }
    //endregion

    //region AppSettings...
    private void AppSettings() {
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            txtUserName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
            txtUserEmail.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
            txtUserReferralCode.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
        txtMyAccount.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtMyAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtOrderHistory.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtChangePWD.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtLogout.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtReviews.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        mTxtCurrency.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        mTxtCommission.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtwallet.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            llMyAccountHeader.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
    }
    //endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        String userID = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        switch (v.getId()) {
            case R.id.llMyAddress:
                if (userID != null) {
                    mListener = (OnFragmentInteractionListener) getContext();
                    mListener.gotoMyAccountAddressListing();
                } else {
                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                    startActivity(intent);
                }

                break;
            case R.id.llMyAccount:
                userID = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
                if (userID != null) {
                    mListener = (OnFragmentInteractionListener) getContext();
                    mListener.gotoUserProfile();
                } else {
                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                    startActivity(intent);
                }
                break;
            case R.id.llOrderHistory:
                if (userID != null) {
                    mListener = (OnFragmentInteractionListener) getContext();
                    mListener.gotoOrderHistory();
                } else {
                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                    startActivity(intent);
                }
                break;
            case R.id.llChangePWD:
                if (userID != null) {
                    mListener = (OnFragmentInteractionListener) getContext();
                    mListener.gotoChangepwd();
                } else {
                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                    startActivity(intent);
                }
                break;
            case R.id.llReviews:
                if (userID != null) {
                    mListener = (OnFragmentInteractionListener) getContext();
                    mListener.gotoReviews();
                } else {
                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                    startActivity(intent);
                }
                break;
            case R.id.llLogout:
                if(txtLogout.getText().toString().equalsIgnoreCase("log in") ||
                        (SharedPreference.GetPreference(getContext(),
                                StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOG_IN) != null &&
                                SharedPreference.GetPreference(getContext(),
                                        StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOG_IN)
                                        .equalsIgnoreCase("") && txtLogout.getText().toString().equalsIgnoreCase
                                (SharedPreference.GetPreference(getContext(),
                                        StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOG_IN)))){
                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    intent.putExtra("Redirect", "myaccount");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }else {
                    if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                        txtLogout.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                        imageLogout.setImageResource(R.drawable.ic_logout);
                        imageLogout.setColorFilter(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                    } else {
                        imageLogout.setImageResource(R.drawable.ic_logout_select);
                    }
                    llLogout.setEnabled(false);
                    Logout();
                }
                break;

            case R.id.llCurrency:
                Intent intentCurrency = new Intent(getActivity(), CurrencyScreen.class);
                intentCurrency.putExtra("redirect", "myaccount");
                startActivity(intentCurrency);
                break;
            case R.id.llCommission:
                Intent intentCommission = new Intent(getActivity(), CommissionScreen.class);
                intentCommission.putExtra("redirect", "myaccount");
                startActivity(intentCommission);
                break;
        }
    }

    // region FOR get Bingage member Info API..
    private void getbingageMemberInfo() {
        relativeProgress.setVisibility(View.VISIBLE);
        String[] key = {};
        String[] val = {};
        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.Bingage_Member_Info);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Global.HideSystemKeyboard(getActivity());
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @SuppressLint("DefaultLocale")
                        @Override
                        public void run() {
                            relativeProgress.setVisibility(View.GONE);
                            if(response.optJSONObject("payload").has("balance")){
                                displayredeembalance( response.optJSONObject("payload").optString
                                        ("balance"));
                                    /*if (SharedPreference.GetPreference(getActivity(), Global.PREFERENCECURRENCY,
                                            StaticUtility.sCurrencySignPosition).equals("1")) {
                                        if (SharedPreference.GetPreference(getContext(),
                                                StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.wallet_balance)
                                                != null) {
                                            if (!SharedPreference.GetPreference(getContext(),
                                                    StaticUtility.MULTILANGUAGEPREFERENCE,
                                                    StaticUtility.wallet_balance).equals("")) {
                                                txtwallet.setText(String.format("%s %s %s",
                                                        SharedPreference.GetPreference(getContext(),
                                                                StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility
                                                                        .wallet_balance), SharedPreference.
                                                                GetPreference(getActivity(),
                                                                        Global.PREFERENCECURRENCY,
                                                                        StaticUtility.sCurrencySign),
                                                        new DecimalFormat("00.00").format(Double.valueOf(
                                                                response.optJSONObject("payload").optString
                                                                        ("balance")))));
                                            } else {
                                                txtwallet.setText(String.format("%d %s %s", R.string.wallet_balance,
                                                        SharedPreference.GetPreference(getActivity(),
                                                        Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign),
                                                        new DecimalFormat("00.00").format(Double.valueOf(
                                                                response.optJSONObject("payload").optString
                                                                        ("balance")))));
                                            }
                                        } else {
                                            txtwallet.setText(String.format("%d %s %s", R.string.wallet_balance,
                                                    SharedPreference.GetPreference(getActivity(),
                                                    Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign),
                                                    new DecimalFormat("00.00").format(Double.valueOf(
                                                            response.optJSONObject("payload").optString
                                                                    ("balance")))));
                                        }
                                    } else if (SharedPreference.GetPreference(getActivity(), Global.PREFERENCECURRENCY,
                                            StaticUtility.sCurrencySignPosition).equals("0")) {
                                        if (SharedPreference.GetPreference(getContext(),
                                                StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.wallet_balance)
                                                != null) {
                                            if (!SharedPreference.GetPreference(getContext(),
                                                    StaticUtility.MULTILANGUAGEPREFERENCE,
                                                    StaticUtility.wallet_balance).equals("")) {
                                                txtwallet.setText(String.format("%s %s %s", SharedPreference.
                                                        GetPreference(getContext(), StaticUtility.
                                                                MULTILANGUAGEPREFERENCE, StaticUtility.wallet_balance),
                                                        new DecimalFormat("00.00").format(Double.valueOf(
                                                                response.optJSONObject("payload").optString
                                                                        ("balance"))),
                                                        SharedPreference.GetPreference(getActivity(),
                                                                Global.PREFERENCECURRENCY,StaticUtility.sCurrencySign)));
                                            } else {
                                                txtwallet.setText(String.format("%d %s %s", R.string.wallet_balance,
                                                        new DecimalFormat("00.00").format(Double.valueOf(
                                                                response.optJSONObject("payload").optString
                                                                        ("balance"))),
                                                        SharedPreference.GetPreference(getActivity(),
                                                        Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign)));
                                            }
                                        } else {
                                            txtwallet.setText(String.format("%d %s %s", R.string.wallet_balance,
                                                    new DecimalFormat("00.00").format(Double.valueOf(
                                                            response.optJSONObject("payload").optString
                                                                    ("balance"))),
                                                    SharedPreference.GetPreference(getActivity(),
                                                            Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign)));
                                        }
                                    }*/

                            }else {
                                displayredeembalance("00.00");
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Global.HideSystemKeyboard(getActivity());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            relativeProgress.setVisibility(View.GONE);
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    //Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT,
                                    "Getting error in MyAccountFragment.java When parsing Error response.\n"
                                            + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region for display redeem balance
    public void displayredeembalance(String balance){
        if (SharedPreference.GetPreference(getActivity(), Global.PREFERENCECURRENCY,
                StaticUtility.sCurrencySignPosition).equals("1")) {
            if (SharedPreference.GetPreference(getContext(),
                    StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.wallet_balance)
                    != null) {
                if (!SharedPreference.GetPreference(getContext(),
                        StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.wallet_balance).equals("")) {
                    txtwallet.setText(String.format("%s %s %s",
                            SharedPreference.GetPreference(getContext(),
                                    StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility
                                            .wallet_balance), SharedPreference.
                                    GetPreference(getActivity(),
                                            Global.PREFERENCECURRENCY,
                                            StaticUtility.sCurrencySign),
                            new DecimalFormat("00.00").format(Double.valueOf(balance))));
                } else {
                    txtwallet.setText(String.format("%d %s %s", R.string.wallet_balance,
                            SharedPreference.GetPreference(getActivity(),
                                    Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign),
                            new DecimalFormat("00.00").format(Double.valueOf(balance))));
                }
            } else {
                txtwallet.setText(String.format("%d %s %s", R.string.wallet_balance,
                        SharedPreference.GetPreference(getActivity(),
                                Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign),
                        new DecimalFormat("00.00").format(Double.valueOf(balance))));
            }
        } else if (SharedPreference.GetPreference(getActivity(), Global.PREFERENCECURRENCY,
                StaticUtility.sCurrencySignPosition).equals("0")) {
            if (SharedPreference.GetPreference(getContext(),
                    StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.wallet_balance)
                    != null) {
                if (!SharedPreference.GetPreference(getContext(),
                        StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.wallet_balance).equals("")) {
                    txtwallet.setText(String.format("%s %s %s", SharedPreference.
                                    GetPreference(getContext(), StaticUtility.
                                            MULTILANGUAGEPREFERENCE, StaticUtility.wallet_balance),
                            new DecimalFormat("00.00").format(Double.valueOf(balance)),
                            SharedPreference.GetPreference(getActivity(),
                                    Global.PREFERENCECURRENCY,StaticUtility.sCurrencySign)));
                } else {
                    txtwallet.setText(String.format("%d %s %s", R.string.wallet_balance,
                            new DecimalFormat("00.00").format(Double.valueOf(balance)),
                            SharedPreference.GetPreference(getActivity(),
                                    Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign)));
                }
            } else {
                txtwallet.setText(String.format("%d %s %s", R.string.wallet_balance,
                        new DecimalFormat("00.00").format(Double.valueOf(balance)),
                        SharedPreference.GetPreference(getActivity(),
                                Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign)));
            }
        }
    }
    //endregion

    //region FOR Logout API..
    private void Logout() {

        String[] key = {"user_token"};
        String[] val = {String.valueOf(SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN))};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.Logout);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                llLogout.setEnabled(true);
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                                SharedPreference.ClearPreference(getContext(), StaticUtility.PREFERENCEREFERRALCODE);
                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                intent.putExtra("Redirect", "logout");
                               /* intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);*/
                                startActivity(intent);
                                /*getActivity().finish();*/
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            llLogout.setEnabled(false);
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strMessage.equalsIgnoreCase("user token mismatched!")) {
                                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED) != null) {
                                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED).equals("")) {
                                            Toast.makeText(getContext(), SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    }
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    /*finish();*/
                                } else {
                                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED) != null) {
                                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED).equals("")) {
                                            Toast.makeText(getContext(), SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    }
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.ISCheck);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    /*finish();*/
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in MyAccountFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void gotoReviews();

        void gotoOrderHistory();

        void gotoUserProfile();

        void gotoChangepwd();

        void gotoMyAccountAddressListing();
    }
}
