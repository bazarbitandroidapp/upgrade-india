package com.pureweblopment.upgradeindia.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.borjabravo.readmoretextview.ReadMoreTextView;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.plus.PlusShare;
import com.pureweblopment.upgradeindia.Activity.LoginActivity;
import com.pureweblopment.upgradeindia.Activity.MainActivity;
import com.pureweblopment.upgradeindia.Adapter.AdapterReviewsList;
import com.pureweblopment.upgradeindia.Adapter.FullViewImageAdapter;
import com.pureweblopment.upgradeindia.Adapter.ProductDetailViewPagerAdapter;
import com.pureweblopment.upgradeindia.Global.ExtendedViewPager;
import com.pureweblopment.upgradeindia.Global.Global;
import com.pureweblopment.upgradeindia.Global.SendMail;
import com.pureweblopment.upgradeindia.Global.SharedPreference;
import com.pureweblopment.upgradeindia.Global.StaticUtility;
import com.pureweblopment.upgradeindia.Global.Typefaces;
import com.pureweblopment.upgradeindia.Linkedin.APIHelper;
import com.pureweblopment.upgradeindia.Linkedin.LISessionManager;
import com.pureweblopment.upgradeindia.Linkedin.errors.LIApiError;
import com.pureweblopment.upgradeindia.Linkedin.errors.LIAuthError;
import com.pureweblopment.upgradeindia.Linkedin.listeners.ApiListener;
import com.pureweblopment.upgradeindia.Linkedin.listeners.ApiResponse;
import com.pureweblopment.upgradeindia.Linkedin.listeners.AuthListener;
import com.pureweblopment.upgradeindia.Linkedin.utils.Scope;
import com.pureweblopment.upgradeindia.Model.DefaultVariation;
import com.pureweblopment.upgradeindia.Model.Reviews;
import com.pureweblopment.upgradeindia.Model.Sliders;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;

import com.pureweblopment.upgradeindia.R;

import me.relex.circleindicator.CircleIndicator;
import okhttp3.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ProductDetailFragment extends Fragment implements View.OnClickListener,
        TextWatcher, ProductDetailViewPagerAdapter.OnClickEvent {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    Bundle bundle;

    ViewPager ViewPagerSlider;
    TextView txtAddtoWishlist, txtAddtoCart, txtProductName, txtProductSortDes,
            txt_product_base_price,
            textProductSalePrice, txtisAvalible;
    RatingBar reviewRatingbar;
    LinearLayout llCard, llVariation, llReviews;

    String[] img;
    int currentPage = 0;
    Timer timer;

    RelativeLayout relativeProgress;

    CircleIndicator indicator;
    String strSlug = "";
    Spinner spinnerVariation;

    TextView txtReturndays, txtCashondelivery, txtExpectedDelivery, txtCheckOption;
    ImageView imageExpectedDelivery, imageCashondelivery, imageReturndays;

    ReadMoreTextView txtProductDetail;
    TextView txtProductDetailTitle, txtRelatedProduct, txtReviewsTitle;

    EditText editTextPincode;
    Button btnCheck;

    int intSalePrice, intBasePrice;
    String strSalePrice;

    JSONArray jsonArrayMainImage, jsonArrayGelleryImage;

    RecyclerView recyclerviewReletedProduct, recyclerviewReviews;
    String strProductDes = "";

    private static String is_wishlist = "", strProductID = "",
            strProductName = "", strShortDescription = "", strWebSiteURL = "",
            strMainProductID = "", strProductSKU = "", strBasePrice = "", strImage = "";
    int Stock_Status = 0, quantity = 0;

    ArrayList<Reviews> reviewses;

    ImageView imageCartBack, imageNavigation, imageLogo;
    FrameLayout frameLayoutCart;
    TextView txtCatName;
    LinearLayout llBottomNavigation;
    RecyclerView recyclerviewVariation;

    private OnFragmentInteractionListener mListener;
    JSONArray jsonArrayVariation = null;
    String productVariationid = "", Variationattritem_name = "";
    ArrayList<String> productIDs = new ArrayList<>(), variationAttribute = new ArrayList<>();

    ArrayList<DefaultVariation> defaultVariations;
    int VariationObject = 0;
    CardView cardviewBottomNavigation;
    View viewVariation;
    FrameLayout framLayoutProductDetail;
    FragmentManager fragmentManager;

    int count = 0;

    JSONArray attrArray = new JSONArray();
    JSONArray attrtermArray = new JSONArray();

    String[] strArrayAttr, strArrayAttrTerm, strAttrTerm;

    ArrayList<String> arrayListAttribute;
    ArrayList<String> arrayListAttributeTerm;

    String strPincodeMessage = "";

    LinearLayout llRelatedProduct;

    Handler handler = new Handler();
    Runnable Update;


    public ProductDetailFragment() {
    }

    JSONArray jsonArrayRelatedProduct;

    //Facebook sharing
    private ShareDialog shareDialog;

    private CardView mCvShare;

    ArrayList<Sliders> sliders;
    private Dialog mFullViewImageDialog;

    private LinearLayout mLlCODAvailable;
    private FrameLayout mFlCheckPincode;

    public static ProductDetailFragment newInstance(String param1, String param2) {
        ProductDetailFragment fragment = new ProductDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity.manageBackPress(false);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_detail, container, false);

        Timer timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 500, 3000);

        shareDialog = new ShareDialog(this);
        ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        bundle = getArguments();
        strSlug = bundle.getString("slug");
        strProductName = bundle.getString("name");
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHECK_PINCODE_AVAILABILITY) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHECK_PINCODE_AVAILABILITY).equals("")) {
                strPincodeMessage = SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHECK_PINCODE_AVAILABILITY);
            } else {
                strPincodeMessage = getString(R.string.please_enter_pincode_and_check_delivery_available_or_not);
            }
        } else {
            strPincodeMessage = getString(R.string.please_enter_pincode_and_check_delivery_available_or_not);
        }

        imageCartBack = getActivity().findViewById(R.id.imageCartBack);
        imageNavigation = getActivity().findViewById(R.id.imageNavigation);
        imageLogo = getActivity().findViewById(R.id.imageLogo);

        frameLayoutCart = getActivity().findViewById(R.id.frameLayoutCart);
        txtCatName = getActivity().findViewById(R.id.txtCatName);
        llBottomNavigation = getActivity().findViewById(R.id.llBottomNavigation);
        cardviewBottomNavigation = (CardView) getActivity().findViewById(R.id.cardviewBottomNavigation);

        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        imageCartBack.setVisibility(View.VISIBLE);
        frameLayoutCart.setVisibility(View.VISIBLE);
        txtCatName.setVisibility(View.VISIBLE);
        txtCatName.setText(strProductName);

        imageNavigation.setVisibility(View.GONE);
        imageLogo.setVisibility(View.GONE);
//        llBottomNavigation.setVisibility(View.GONE);
        cardviewBottomNavigation.setVisibility(View.GONE);

        imageCartBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    getActivity().onBackPressed();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

        MainActivity.ClearSharePreference = true;

        Initialization(view);
        TypeFace();
        OnClickListener();
        AppSettings();
        setDynamicLabel();

        chanageEditTextBorder(editTextPincode);
        chanageButton(btnCheck);

        is_wishlist = SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active);

        /*if (SharedPreference.GetPreference(getContext(), StaticUtility.PREFERENCEPAYMENTGATETWAY, StaticUtility.sCODAVAILABLE) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.PREFERENCEPAYMENTGATETWAY, StaticUtility.sCODAVAILABLE).equals("")) {
                String strCODAvailable = SharedPreference.GetPreference(getContext(), StaticUtility.PREFERENCEPAYMENTGATETWAY, StaticUtility.sCODAVAILABLE);
                if (strCODAvailable.equals("1")) {
                    mLlCODAvailable.setVisibility(View.VISIBLE);
                } else {
                    mLlCODAvailable.setVisibility(View.GONE);
                }
            } else {
                mLlCODAvailable.setVisibility(View.GONE);
            }
        } else {
            mLlCODAvailable.setVisibility(View.GONE);
        }*/


        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CHECK_PINCODE_AVAILABILITY) != null) {
            if (!SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CHECK_PINCODE_AVAILABILITY).equals("")) {
                String strCheckPincodeAvailability = SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CHECK_PINCODE_AVAILABILITY);
                if (strCheckPincodeAvailability.equals("1")) {
                    mFlCheckPincode.setVisibility(View.VISIBLE);
                } else {
                    mFlCheckPincode.setVisibility(View.GONE);
                }
            } else {
                mFlCheckPincode.setVisibility(View.GONE);
            }
        } else {
            mFlCheckPincode.setVisibility(View.GONE);
        }

        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Cart_Active).equals("1")) {
            llCard.setVisibility(View.VISIBLE);
            txtAddtoCart.setVisibility(View.VISIBLE);
            frameLayoutCart.setVisibility(View.VISIBLE);
        } else {
            txtAddtoCart.setVisibility(View.GONE);
            frameLayoutCart.setVisibility(View.GONE);
        }

        getProductDetails(strSlug);

        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, StaticUtility.sPincode) != null) {
            String strPincode = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, StaticUtility.sPincode);
            editTextPincode.setText(strPincode);
        } else {
            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sENTER_PINCODE) != null) {
                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sENTER_PINCODE).equals("")) {
                    editTextPincode.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sENTER_PINCODE));
                } else {
                    editTextPincode.setHint(getString(R.string.enter_pincode));
                }
            } else {
                editTextPincode.setHint(getString(R.string.enter_pincode));
            }
        }

        return view;
    }

    //region Initialization
    private void Initialization(View view) {
        ViewPagerSlider = (ViewPager) view.findViewById(R.id.ViewPagerSlider);
        txtAddtoWishlist = view.findViewById(R.id.txtAddtoWishlist);
        txtAddtoCart = view.findViewById(R.id.txtAddtoCart);
        llCard = view.findViewById(R.id.llCard);
        relativeProgress = (RelativeLayout) view.findViewById(R.id.relativeProgress);
        indicator = (CircleIndicator) view.findViewById(R.id.indicator);

        txtProductName = view.findViewById(R.id.txtProductName);
        txtProductSortDes = view.findViewById(R.id.txtProductSortDes);
        txt_product_base_price = view.findViewById(R.id.txt_product_base_price);
        textProductSalePrice = view.findViewById(R.id.textProductSalePrice);
//        txtTitleVariation = view.findViewById(R.id.txtTitleVariation);
//        txtTitleVariationColor = view.findViewById(R.id.txtTitleVariationColor);

        txtisAvalible = view.findViewById(R.id.txtisAvalible);
        txtCheckOption = view.findViewById(R.id.txtCheckOption);

        txtRelatedProduct = view.findViewById(R.id.txtRelatedProduct);
        txtReviewsTitle = view.findViewById(R.id.txtReviewsTitle);

        editTextPincode = view.findViewById(R.id.editTextPincode);
        btnCheck = view.findViewById(R.id.btnCheck);

        llVariation = view.findViewById(R.id.llVariation);
        llReviews = view.findViewById(R.id.llReviews);

        spinnerVariation = view.findViewById(R.id.spinnerVariation);

        txtReturndays = view.findViewById(R.id.txtReturndays);
        txtCashondelivery = view.findViewById(R.id.txtCashondelivery);
        txtExpectedDelivery = view.findViewById(R.id.txtExpectedDelivery);

        imageExpectedDelivery = view.findViewById(R.id.imageExpectedDelivery);
        imageCashondelivery = view.findViewById(R.id.imageCashondelivery);
        imageReturndays = view.findViewById(R.id.imageReturndays);

        txtProductDetail = view.findViewById(R.id.txtProductDetail);
        txtProductDetailTitle = view.findViewById(R.id.txtProductDetailTitle);

        viewVariation = view.findViewById(R.id.viewVariation);

        recyclerviewReletedProduct = view.findViewById(R.id.recyclerviewReletedProduct);
        recyclerviewReviews = view.findViewById(R.id.recyclerviewReviews);
      /*  recyclerviewSize = view.findViewById(R.id.recyclerviewSize);
        recyclerviewColor = view.findViewById(R.id.recyclerviewColor);*/
        recyclerviewVariation = view.findViewById(R.id.recyclerviewVariation);
        framLayoutProductDetail = view.findViewById(R.id.framLayoutProductDetail);

        reviewRatingbar = view.findViewById(R.id.reviewRatingbar);
        LayerDrawable stars = (LayerDrawable) reviewRatingbar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

        llRelatedProduct = view.findViewById(R.id.llRelatedProduct);
        mCvShare = view.findViewById(R.id.cvShare);
        mLlCODAvailable = view.findViewById(R.id.llCODAvailable);
        mFlCheckPincode = view.findViewById(R.id.flCheckPincode);
    }
    //endregion

    //region OnClickListener
    private void OnClickListener() {
        txtAddtoCart.setOnClickListener(this);
        txtAddtoWishlist.setOnClickListener(this);
        btnCheck.setOnClickListener(this);
        mCvShare.setOnClickListener(this);
        editTextPincode.addTextChangedListener(this);
    }
    //endregion

    //region TypeFace
    private void TypeFace() {
        txtAddtoCart.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
        txtAddtoWishlist.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
        txtProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtProductSortDes.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txt_product_base_price.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        textProductSalePrice.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
//        txtTitleVariation.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
//        txtTitleVariationColor.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtExpectedDelivery.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtCashondelivery.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtReturndays.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtProductDetail.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtProductDetailTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtRelatedProduct.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtReviewsTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
    }
    //endregion

    //region AppSettings
    private void AppSettings() {
        txtProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        textProductSalePrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txt_product_base_price.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtProductSortDes.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
//        txtTitleVariation.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TEXTCOLOR)));
//        txtTitleVariationColor.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TEXTCOLOR)));
        txtReturndays.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtCashondelivery.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtExpectedDelivery.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtProductDetailTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtProductDetail.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
//        txtAddtoCart.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.EDITTEXTCOLOR)));
//        txtAddtoWishlist.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.EDITTEXTCOLOR)));

        txtAddtoCart.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtAddtoCart.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
//        txtAddtoWishlist.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtAddtoWishlist.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtCheckOption.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtRelatedProduct.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtReviewsTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        mCvShare.setCardBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
    }
    //endregion

    private void setDynamicLabel() {
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHECK_DELIVERY_OPTION) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHECK_DELIVERY_OPTION).equals("")) {
                txtCheckOption.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHECK_DELIVERY_OPTION));
            } else {
                txtCheckOption.setText(getString(R.string.check_delivery_option));
            }
        } else {
            txtCheckOption.setText(getString(R.string.check_delivery_option));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHECK) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHECK).equals("")) {
                btnCheck.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHECK));
            } else {
                btnCheck.setText(getString(R.string.check));
            }
        } else {
            btnCheck.setText(getString(R.string.check));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPRODUCT_DETAILS) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPRODUCT_DETAILS).equals("")) {
                txtProductDetailTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPRODUCT_DETAILS));
            } else {
                txtProductDetailTitle.setText(getString(R.string.product_details));
            }
        } else {
            txtProductDetailTitle.setText(getString(R.string.product_details));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRELATED_PRODUCT) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRELATED_PRODUCT).equals("")) {
                txtRelatedProduct.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRELATED_PRODUCT));
            } else {
                txtRelatedProduct.setText(getString(R.string.releted_product));
            }
        } else {
            txtRelatedProduct.setText(getString(R.string.releted_product));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sENTER_PINCODE) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sENTER_PINCODE).equals("")) {
                editTextPincode.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sENTER_PINCODE));
            } else {
                editTextPincode.setHint(getString(R.string.enter_pincode));
            }
        } else {
            editTextPincode.setHint(getString(R.string.enter_pincode));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPOPULAR_REVIEWS) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPOPULAR_REVIEWS).equals("")) {
                txtReviewsTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPOPULAR_REVIEWS));
            } else {
                txtReviewsTitle.setText(getString(R.string.popular_reviews));
            }
        } else {
            txtReviewsTitle.setText(getString(R.string.popular_reviews));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADD_TO_WISHLIST) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADD_TO_WISHLIST).equals("")) {
                txtAddtoWishlist.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADD_TO_WISHLIST));
            } else {
                txtAddtoWishlist.setText(getString(R.string.addtowishlist));
            }
        } else {
            txtAddtoWishlist.setText(getString(R.string.addtowishlist));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCASH_ON_DELIVERY) != null) {
            txtCashondelivery.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCASH_ON_DELIVERY));
        } else {
            txtCashondelivery.setText(getString(R.string.cash_on_delivery));
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        strProductID = "";
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtAddtoCart:
                if (txtAddtoCart.getText().toString().equalsIgnoreCase(getString(R.string.addtocart))) {
                    if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CHECK_PINCODE_AVAILABILITY) != null) {
                        if (!SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CHECK_PINCODE_AVAILABILITY).equals("")) {
                            String strCheckPincodeAvailability = SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.CHECK_PINCODE_AVAILABILITY);
                            if (strCheckPincodeAvailability.equals("1")) {
                                if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, StaticUtility.sPincode) != null) {
                                    String strPincode = SharedPreference.GetPreference(getContext(),
                                            Global.LOGIN_PREFERENCE, StaticUtility.sPincode);
                                    if (strPincode.equalsIgnoreCase(editTextPincode.getText().toString())) {
                                        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERY_AVAILABLE) != null) {
                                            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERY_AVAILABLE).equals("")) {
                                                txtisAvalible.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERY_AVAILABLE));
                                            } else {
                                                txtisAvalible.setText(getString(R.string.delivery_available));
                                            }
                                        } else {
                                            txtisAvalible.setText(getString(R.string.delivery_available));
                                        }
                                        txtisAvalible.setTextColor(Color.parseColor("#008000"));
                                        imageCashondelivery.setImageResource(R.drawable.ic_right);
                                        if (!strProductID.equalsIgnoreCase("")) {
                                            AddToCart(strProductID);
                                        } else {
                                            AddToCart(strMainProductID);
                                        }
                                    } else {
                                        Toast.makeText(getContext(), strPincodeMessage, Toast.LENGTH_SHORT).show();
                                        /*Toast.makeText(getContext(), "Please enter onther pincode because delivery not available!", Toast.LENGTH_SHORT).show();*/
                                    }
                                } else {
                                    Toast.makeText(getContext(), strPincodeMessage, Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                if (!strProductID.equalsIgnoreCase("")) {
                                    AddToCart(strProductID);
                                } else {
                                    AddToCart(strMainProductID);
                                }
                            }
                        } else {
                            if (!strProductID.equalsIgnoreCase("")) {
                                AddToCart(strProductID);
                            } else {
                                AddToCart(strMainProductID);
                            }
                        }

                    } else {
                        if (!strProductID.equalsIgnoreCase("")) {
                            AddToCart(strProductID);
                        } else {
                            AddToCart(strMainProductID);
                        }
                    }
                } else if (txtAddtoCart.getText().toString().equalsIgnoreCase(getString(R.string.notify_me))) {
                    if (!strProductID.equalsIgnoreCase("")) {
                        CheckItemInNofityMe(strProductID);
                    } else {
                        CheckItemInNofityMe(strMainProductID);
                    }
                } else if (txtAddtoCart.getText().toString().equalsIgnoreCase(getString(R.string.go_to_cart))) {
                    productIDs.clear();
                    variationAttribute.clear();
                    SharedPreference.ClearPreference(getContext(), Global.IsClickPreference);
                    mListener = (OnFragmentInteractionListener) getContext();
                    mListener.GoToCartList();
                } else if (txtAddtoCart.getText().toString().equalsIgnoreCase(getString(R.string.pre_booking))) {
                    if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, StaticUtility.sPincode) != null) {
                        String strPincode = SharedPreference.GetPreference(getContext(),
                                Global.LOGIN_PREFERENCE, StaticUtility.sPincode);
                        if (strPincode.equalsIgnoreCase(editTextPincode.getText().toString())) {
                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERY_AVAILABLE) != null) {
                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERY_AVAILABLE).equals("")) {
                                    txtisAvalible.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERY_AVAILABLE));
                                } else {
                                    txtisAvalible.setText(getString(R.string.delivery_available));
                                }
                            } else {
                                txtisAvalible.setText(getString(R.string.delivery_available));
                            }
                            txtisAvalible.setTextColor(Color.parseColor("#008000"));
                            imageCashondelivery.setImageResource(R.drawable.ic_right);
                            if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID) != null) {
                                if (!strProductID.equalsIgnoreCase("")) {
                                    PreBooking(strProductID);
                                } else {
                                    PreBooking(strMainProductID);
                                }
                            } else {
                                Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                startActivity(intent);
                            }
                        } else {
                            Toast.makeText(getContext(), strPincodeMessage, Toast.LENGTH_SHORT).show();
                            /*Toast.makeText(getContext(), "Please enter onther pincode because delivery not available!", Toast.LENGTH_SHORT).show();*/
                        }
                    } else {
                        Toast.makeText(getContext(), strPincodeMessage, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.txtAddtoWishlist:
                if (!strProductID.equalsIgnoreCase("")) {
                    AddToWishListProduct(strProductID);
                } else {
                    AddToWishListProduct(strMainProductID);
                }
                break;
            case R.id.btnCheck:
                if (!TextUtils.isEmpty(editTextPincode.getText().toString())) {
                    CheckPincode();
                } else {
                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPINCODE_REQUIRED) != null) {
                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPINCODE_REQUIRED).equals("")) {
                            Toast.makeText(getContext(), SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPINCODE_REQUIRED), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), getString(R.string.pincode_required), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.pincode_required), Toast.LENGTH_SHORT).show();
                    }
                }
                hideKeyboard(getActivity());
                break;

            case R.id.cvShare:
                ShareDialog();
                break;
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() > 0) {
            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHECK_DELIVERY_OPTION_AVAILABLE_OR_NOT) != null) {
                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHECK_DELIVERY_OPTION_AVAILABLE_OR_NOT).equals("")) {
                    strPincodeMessage = SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHECK_DELIVERY_OPTION_AVAILABLE_OR_NOT);
                } else {
                    strPincodeMessage = getString(R.string.check_delivery_available_or_not);
                }
            } else {
                strPincodeMessage = getString(R.string.check_delivery_available_or_not);
            }
        } else {
            txtisAvalible.setVisibility(View.GONE);
            strPincodeMessage = getString(R.string.please_enter_pincode_and_check_delivery_available_or_not);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {


    }

    @Override
    public void FullViewImage(int position) {
        if (mFullViewImageDialog != null) {
            if (!mFullViewImageDialog.isShowing()) {
                FullViewImageDialog(position);
            }
        } else {
            FullViewImageDialog(position);
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void onAddToCartProduct();

        void GoToCartList();

        void WishlistCount();

        void gotoCheckoutCart(Bundle bundle);
    }

    //region ViewPager...
    private void getViewPager(final ViewPager viewPager) {
//        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getActivity(), sliderses);
        ProductDetailViewPagerAdapter viewPagerAdapter = new ProductDetailViewPagerAdapter(getContext(), ProductDetailFragment.this, img);
        viewPager.setAdapter(viewPagerAdapter);
        if (img.length > 1) {
            indicator.setVisibility(View.VISIBLE);
            indicator.setViewPager(viewPager);
        } else {
            indicator.setVisibility(View.GONE);
        }

        Update = new Runnable() {
            public void run() {
                if (currentPage == img.length) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };
    }
    //endregion

    //region FOR getProductDetails API...
    private void getProductDetails(final String slug) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;

        String user_id = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        String strCurrencyCode = SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencyCode);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            key = new String[]{"slug", "user_id", "currencyCode"};
            val = new String[]{slug, user_id, strCurrencyCode};
        } else {
            key = new String[]{"slug", "user_id", "currencyCode"};
            val = new String[]{slug, "", strCurrencyCode};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetSigalProduct);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
//                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    strWebSiteURL = jsonObjectPayload.getString("websiteurl");
                                    if (jsonObjectPayload.has("products")) {
                                        JSONObject jsonObjectProducts = jsonObjectPayload.getJSONObject("products");
                                        String product_type = jsonObjectProducts.getString("product_type");
                                        /*strProductID = jsonObjectProducts.getString("product_id");*/
                                        strProductName = jsonObjectProducts.getString("name");
                                        strProductSKU = jsonObjectProducts.getString("sku");
                                        strShortDescription = jsonObjectProducts.getString("short_description");
                                        txtProductSortDes.setText(strShortDescription);
                                        /*txtCatName.setText(strProductName);*/
                                        txtProductName.setText(strProductName);

                                        String expected_delivery_time = jsonObjectProducts.getString("expected_delivery_time");
                                        String returnDays = jsonObjectProducts.getString("return_days");
                                        strProductDes = jsonObjectProducts.getString("description");
//                                        strProductID = jsonObjectProducts.getString("product_id");
                                        strMainProductID = jsonObjectProducts.getString("product_id");
                                        /*quantity = Integer.parseInt(jsonObjectProducts.getString("quantity"));*/

                                        if (!expected_delivery_time.equals("")) {
                                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXPEXCTED_DELIVERY) != null) {
                                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXPEXCTED_DELIVERY).equals("")) {
                                                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDAYS) != null) {
                                                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDAYS).equals("")) {
                                                            txtExpectedDelivery.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXPEXCTED_DELIVERY)
                                                                    + expected_delivery_time + " " + SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDAYS));
                                                        } else {
                                                            txtExpectedDelivery.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXPEXCTED_DELIVERY)
                                                                    + expected_delivery_time + " " + getString(R.string.days));
                                                        }
                                                    } else {
                                                        txtExpectedDelivery.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXPEXCTED_DELIVERY)
                                                                + expected_delivery_time + " " + getString(R.string.days));
                                                    }
                                                } else {
                                                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDAYS) != null) {
                                                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDAYS).equals("")) {
                                                            txtExpectedDelivery.setText(getString(R.string.expected_delivery) + " " + expected_delivery_time
                                                                    + " " + SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDAYS));
                                                        } else {
                                                            txtExpectedDelivery.setText(getString(R.string.expected_delivery) + expected_delivery_time
                                                                    + " " + getString(R.string.days));
                                                        }
                                                    } else {
                                                        txtExpectedDelivery.setText(getString(R.string.expected_delivery) + expected_delivery_time
                                                                + " " + getString(R.string.days));
                                                    }
                                                }
                                            } else {
                                                if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDAYS) != null) {
                                                    if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDAYS).equals("")) {
                                                        txtExpectedDelivery.setText(getString(R.string.expected_delivery) + " " + expected_delivery_time
                                                                + " " + SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDAYS));
                                                    } else {
                                                        txtExpectedDelivery.setText(getString(R.string.expected_delivery) + expected_delivery_time
                                                                + " " + getString(R.string.days));
                                                    }
                                                } else {
                                                    txtExpectedDelivery.setText(getString(R.string.expected_delivery) + expected_delivery_time
                                                            + " " + getString(R.string.days));
                                                }
                                            }
                                            imageExpectedDelivery.setImageResource(R.drawable.ic_right);
                                        } else {
                                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXPEXCTED_DELIVERY) != null) {
                                                txtExpectedDelivery.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXPEXCTED_DELIVERY));
                                            } else {
                                                txtExpectedDelivery.setText(getString(R.string.expected_delivery));
                                            }
                                            imageExpectedDelivery.setImageResource(R.drawable.ic_worrng);
                                        }
                                        if (!returnDays.equals("")) {
                                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDAYS_RETURN_AVAILABLE) != null) {
                                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDAYS_RETURN_AVAILABLE).equals("")) {
                                                    txtReturndays.setText(returnDays + " " + SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDAYS_RETURN_AVAILABLE));
                                                } else {
                                                    txtReturndays.setText(returnDays + getString(R.string.days_returns_available));
                                                }
                                            } else {
                                                txtReturndays.setText(returnDays + getString(R.string.days_returns_available));
                                            }
                                            imageReturndays.setImageResource(R.drawable.ic_right);
                                        } else {
                                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDAYS_RETURN_AVAILABLE) != null) {
                                                if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDAYS_RETURN_AVAILABLE) != null) {
                                                    txtReturndays.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDAYS_RETURN_AVAILABLE));
                                                } else {
                                                    txtReturndays.setText(getString(R.string.days_returns_available));
                                                }
                                            } else {
                                                txtReturndays.setText(getString(R.string.days_returns_available));
                                            }
                                            imageReturndays.setImageResource(R.drawable.ic_worrng);
                                        }

                                        if (product_type.equalsIgnoreCase("variable")) {
                                            getDefualtVariationData(strMainProductID);
                                            JSONArray jsonArrayVariations = jsonObjectProducts.getJSONArray("variations");
                                            for (int i = 0; i < jsonArrayVariations.length(); i++) {
                                                JSONObject jsonObjectVariations = jsonArrayVariations.getJSONObject(i);
                                                String defaultvariation = jsonObjectVariations.getString("defaultvariation");
                                                if (defaultvariation.equals("1")) {
                                                    Stock_Status = Integer.parseInt(jsonObjectVariations.getString("stock_status"));
                                                    quantity = Integer.parseInt(jsonObjectVariations.getString("quantity"));
                                                    strProductID = jsonObjectVariations.getString("product_id");
                                                    strProductName = jsonObjectVariations.getString("name");
                                                    strProductSKU = jsonObjectVariations.getString("sku");
                                                    String strProductDes = jsonObjectVariations.getString("description");
                                                    if (!strProductDes.equals("")) {
                                                        //txtProductDetail.
                                                        txtProductDetail.setText(strProductDes);
                                                    } else {
                                                        txtProductDetail.setText(jsonObjectProducts.getString("description"));
                                                    }
                                                   /* if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREAD_MORE) != null) {
                                                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREAD_MORE).equals("")) {
                                                            Global.MakeTextViewResizable(getContext(), txtProductDetail,
                                                                    3, SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE,
                                                                            StaticUtility.sREAD_MORE), true);
                                                        } else {
                                                            Global.MakeTextViewResizable(getContext(), txtProductDetail, 3, getString(R.string.read_more), true);
                                                        }
                                                    } else {
                                                        Global.MakeTextViewResizable(getContext(), txtProductDetail, 3, getString(R.string.read_more), true);
                                                    }
*/
                                                    /*txtCatName.setText(strProductName);*/

                                                    strBasePrice = jsonObjectVariations.getString("price");
                                                    float floatBasePrice = Float.parseFloat(jsonObjectVariations.getString("price"));
                                                    intBasePrice = Math.round(floatBasePrice);
                                                    strSalePrice = jsonObjectVariations.getString("sale_price");
                                                    float floatSalePrice = Float.parseFloat(strSalePrice);
                                                    intSalePrice = Math.round(floatSalePrice);

                                                    if (intSalePrice > 0) {
                                                        if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                                                                StaticUtility.sCurrencySignPosition).equals("1")) {
                                                            textProductSalePrice.setText(SharedPreference.GetPreference(getContext(),
                                                                    Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign) + " " +
                                                                    jsonObjectVariations.getString("sale_price"));
                                                            txt_product_base_price.setText(SharedPreference.GetPreference(getContext(),
                                                                    Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign) + " " + jsonObjectVariations.getString("price"));
                                                        } else if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                                                                StaticUtility.sCurrencySignPosition).equals("0")) {
                                                            textProductSalePrice.setText(jsonObjectVariations.getString("sale_price") + " " +
                                                                    SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign));
                                                            txt_product_base_price.setText(jsonObjectVariations.getString("price") + " " +
                                                                    SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign));
                                                        }
                                                    } else {
                                                        if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                                                                StaticUtility.sCurrencySignPosition).equals("1")) {
                                                            textProductSalePrice.setText(SharedPreference.GetPreference(getContext(),
                                                                    Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign) + " " +
                                                                    jsonObjectVariations.getString("price"));
                                                            txt_product_base_price.setVisibility(View.GONE);
                                                        } else if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                                                                StaticUtility.sCurrencySignPosition).equals("0")) {
                                                            textProductSalePrice.setText(jsonObjectVariations.getString("price") + " " +
                                                                    SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign));
                                                            txt_product_base_price.setVisibility(View.GONE);
                                                        }
                                                    }
                                                    sliders = new ArrayList<>();
                                                    Object objProducts = jsonObjectVariations.get("gallery_images");
                                                    if (!objProducts.equals("")) {
                                                        jsonArrayGelleryImage = jsonObjectVariations.getJSONArray("gallery_images");
                                                        JSONObject jsonObjectImages = jsonObjectVariations.getJSONObject("main_image");
                                                        strImage = jsonObjectImages.getString("main_image");
                                                        String images = jsonObjectImages.getString("main_image");
                                                        img = new String[jsonArrayGelleryImage.length() + 1];
                                                        img[0] = images;
                                                        sliders.add(new Sliders(0, images));
                                                        for (int j = 0; j < jsonArrayGelleryImage.length(); j++) {
                                                            JSONObject jsonObjectGelleryImages = jsonArrayGelleryImage.getJSONObject(j);
                                                            String GelleryImages = jsonObjectGelleryImages.getString("main_image");
                                                            img[j + 1] = GelleryImages;
                                                            sliders.add(new Sliders(j + 1, GelleryImages));
                                                        }
                                                    } else {
                                                        img = new String[1];
                                                        JSONObject jsonObjectImages = jsonObjectVariations.getJSONObject("main_image");
                                                        String images = jsonObjectImages.getString("main_image");
                                                        strImage = jsonObjectImages.getString("main_image");
                                                        img[0] = images;
                                                        sliders.add(new Sliders(0, images));
                                                    }
                                                    String strSlug = jsonObjectVariations.optString("slug");
                                                    getReviews(strSlug);
                                                }
                                            }

                                            if (jsonObjectProducts.has("usedVariation")) {
                                                llVariation.setVisibility(View.VISIBLE);
                                                viewVariation.setVisibility(View.VISIBLE);
                                                jsonArrayVariation = jsonObjectProducts.getJSONArray("usedVariation");
                                                /*AdapterAttributeTitle adapterAttributeTitle = new AdapterAttributeTitle(getContext(), jsonArrayVariation);
                                                recyclerviewVariation.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                                recyclerviewVariation.setAdapter(adapterAttributeTitle);*/
                                            } else {
                                                llVariation.setVisibility(View.GONE);
                                                viewVariation.setVisibility(View.GONE);
                                            }
                                        } else {
                                            Stock_Status = Integer.parseInt(jsonObjectProducts.getString("stock_status"));
                                            quantity = Integer.parseInt(jsonObjectProducts.getString("quantity"));
                                            strBasePrice = jsonObjectProducts.getString("price");
                                            float floatSalePrice = Float.parseFloat(jsonObjectProducts.getString("price"));
                                            intBasePrice = Math.round(floatSalePrice);
                                            strSalePrice = jsonObjectProducts.getString("sale_price");
                                            float floatBasePrice = Float.parseFloat(strSalePrice);
                                            intSalePrice = Math.round(floatBasePrice);
                                            strProductDes = jsonObjectProducts.getString("description");
                                            txtProductDetail.setText(strProductDes);
                                            /*if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREAD_MORE) != null) {
                                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREAD_MORE).equals("")) {
                                                    Global.MakeTextViewResizable(getContext(), txtProductDetail,
                                                            3, SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE,
                                                                    StaticUtility.sREAD_MORE), true);
                                                } else {
                                                    Global.MakeTextViewResizable(getContext(), txtProductDetail, 3, getString(R.string.read_more), true);
                                                }
                                            } else {
                                                Global.MakeTextViewResizable(getContext(), txtProductDetail, 3, getString(R.string.read_more), true);
                                            }*/

                                            if (intSalePrice > 0) {
                                                if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                                                        StaticUtility.sCurrencySignPosition).equals("1")) {
                                                    textProductSalePrice.setText(SharedPreference.GetPreference(getContext(),
                                                            Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign) + " " + jsonObjectProducts.getString("sale_price"));
                                                    txt_product_base_price.setText(SharedPreference.GetPreference(getContext(),
                                                            Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign) + " " + jsonObjectProducts.getString("price"));
                                                } else if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                                                        StaticUtility.sCurrencySignPosition).equals("0")) {
                                                    textProductSalePrice.setText(jsonObjectProducts.getString("sale_price") + " " +
                                                            SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign));
                                                    txt_product_base_price.setText(jsonObjectProducts.getString("price") + " " +
                                                            SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign));
                                                }
                                            } else {
                                                if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                                                        StaticUtility.sCurrencySignPosition).equals("1")) {
                                                    textProductSalePrice.setText(SharedPreference.GetPreference(getContext(),
                                                            Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign) + " " + jsonObjectProducts.getString("price"));
                                                    txt_product_base_price.setVisibility(View.GONE);
                                                } else if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                                                        StaticUtility.sCurrencySignPosition).equals("0")) {
                                                    textProductSalePrice.setText(jsonObjectProducts.getString("price") + " " +
                                                            SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign));
                                                    txt_product_base_price.setVisibility(View.GONE);
                                                }
                                            }
                                            Object objProducts = jsonObjectProducts.get("gallery_images");
                                            sliders = new ArrayList<>();
                                            if (!objProducts.equals("")) {
                                                jsonArrayGelleryImage = jsonObjectProducts.getJSONArray("gallery_images");
                                                JSONObject jsonObjectImages = jsonObjectProducts.getJSONObject("main_image");
                                                strImage = jsonObjectImages.getString("main_image");
                                                String images = jsonObjectImages.getString("main_image");
                                                img = new String[jsonArrayGelleryImage.length() + 1];
                                                img[0] = images;
                                                sliders.add(new Sliders(0, images));
                                                for (int i = 0; i < jsonArrayGelleryImage.length(); i++) {
                                                    JSONObject jsonObjectGelleryImages = jsonArrayGelleryImage.getJSONObject(i);
                                                    String GelleryImages = jsonObjectGelleryImages.getString("main_image");
                                                    img[i + 1] = GelleryImages;
                                                    sliders.add(new Sliders(i + 1, GelleryImages));
                                                }
                                            } else {
                                                img = new String[1];
                                                JSONObject jsonObjectImages = jsonObjectProducts.getJSONObject("main_image");
                                                String images = jsonObjectImages.getString("main_image");
                                                strImage = jsonObjectImages.getString("main_image");
                                                img[0] = images;
                                                sliders.add(new Sliders(0, images));
                                            }
                                        }


                                        String strCurrentDateTime = "", strPreBookingEndDate = "";
                                        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.PRE_BOOKING_ENABLED) != null) {
                                            String strPreBookingEnable = SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.PRE_BOOKING_ENABLED);
                                            if (strPreBookingEnable.equalsIgnoreCase("1")) {
                                                if (jsonObjectProducts.has("pre_booking_enabled")) {
                                                    String strPreBookingEnabled = jsonObjectProducts.optString("pre_booking_enabled");
                                                    if (strPreBookingEnabled.equalsIgnoreCase("1")) {
                                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                                        String strStartDateTime = jsonObjectProducts.optString("pre_booking_startdate");
                                                        strCurrentDateTime = jsonObjectProducts.optString("currentdatetime");
                                                        strPreBookingEndDate = jsonObjectProducts.optString("pre_booking_enddate");
                                                        Date startDate = null, currentDate = null;
                                                        try {
                                                            startDate = sdf.parse(strStartDateTime);
                                                            currentDate = sdf.parse(strCurrentDateTime);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                        if (currentDate.after(startDate)) {
                                                            txtAddtoWishlist.setVisibility(View.GONE);
                                                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPRE_BOOKING) != null) {
                                                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPRE_BOOKING).equals("")) {
                                                                    txtAddtoCart.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPRE_BOOKING));
                                                                } else {
                                                                    txtAddtoCart.setText(getString(R.string.pre_booking));
                                                                }
                                                            } else {
                                                                txtAddtoCart.setText(getString(R.string.pre_booking));
                                                            }
                                                            setTimer(strCurrentDateTime, strPreBookingEndDate);
                                                        } else {
                                                            if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active).equals("1")) {
                                                                llCard.setVisibility(View.VISIBLE);
                                                                txtAddtoWishlist.setVisibility(View.VISIBLE);
                                                            } else {
                                                                txtAddtoWishlist.setVisibility(View.GONE);
                                                            }
                                                            if (Stock_Status == 0 || quantity <= 0) {
                                                                if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME) != null) {
                                                                    if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME).equals("")) {
                                                                        txtAddtoCart.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME));
                                                                    } else {
                                                                        txtAddtoCart.setText(getString(R.string.notify_me));
                                                                    }
                                                                } else {
                                                                    txtAddtoCart.setText(getString(R.string.notify_me));
                                                                }
                                                            } else {
                                                                if (!strProductID.equalsIgnoreCase("")) {
                                                                    CheckItemInCart(strProductID);
                                                                } else {
                                                                    CheckItemInCart(strMainProductID);
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active).equals("1")) {
                                                            llCard.setVisibility(View.VISIBLE);
                                                            txtAddtoWishlist.setVisibility(View.VISIBLE);
                                                        } else {
                                                            txtAddtoWishlist.setVisibility(View.GONE);
                                                        }
                                                        if (Stock_Status == 0 || quantity <= 0) {
                                                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME) != null) {
                                                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME).equals("")) {
                                                                    txtAddtoCart.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME));
                                                                } else {
                                                                    txtAddtoCart.setText(getString(R.string.notify_me));
                                                                }
                                                            } else {
                                                                txtAddtoCart.setText(getString(R.string.notify_me));
                                                            }
                                                        } else {
                                                            if (!strProductID.equalsIgnoreCase("")) {
                                                                CheckItemInCart(strProductID);
                                                            } else {
                                                                CheckItemInCart(strMainProductID);
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active).equals("1")) {
                                                        llCard.setVisibility(View.VISIBLE);
                                                        txtAddtoWishlist.setVisibility(View.VISIBLE);
                                                    } else {
                                                        txtAddtoWishlist.setVisibility(View.GONE);
                                                    }
                                                    if (Stock_Status == 0 || quantity <= 0) {
                                                        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME) != null) {
                                                            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME).equals("")) {
                                                                txtAddtoCart.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME));
                                                            } else {
                                                                txtAddtoCart.setText(getString(R.string.notify_me));
                                                            }
                                                        } else {
                                                            txtAddtoCart.setText(getString(R.string.notify_me));
                                                        }
                                                    } else {
                                                        if (!strProductID.equalsIgnoreCase("")) {
                                                            CheckItemInCart(strProductID);
                                                        } else {
                                                            CheckItemInCart(strMainProductID);
                                                        }
                                                    }
                                                }
                                            } else {
                                                if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active).equals("1")) {
                                                    llCard.setVisibility(View.VISIBLE);
                                                    txtAddtoWishlist.setVisibility(View.VISIBLE);
                                                } else {
                                                    txtAddtoWishlist.setVisibility(View.GONE);
                                                }
                                                if (Stock_Status == 0 || quantity <= 0) {
                                                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME) != null) {
                                                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME).equals("")) {
                                                            txtAddtoCart.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME));
                                                        } else {
                                                            txtAddtoCart.setText(getString(R.string.notify_me));
                                                        }
                                                    } else {
                                                        txtAddtoCart.setText(getString(R.string.notify_me));
                                                    }
                                                } else {
                                                    if (!strProductID.equalsIgnoreCase("")) {
                                                        CheckItemInCart(strProductID);
                                                    } else {
                                                        CheckItemInCart(strMainProductID);
                                                    }
                                                }
                                            }
                                        } else {
                                            if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active).equals("1")) {
                                                llCard.setVisibility(View.VISIBLE);
                                                txtAddtoWishlist.setVisibility(View.VISIBLE);
                                            } else {
                                                txtAddtoWishlist.setVisibility(View.GONE);
                                            }
                                            if (Stock_Status == 0 || quantity <= 0) {
                                                if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME) != null) {
                                                    if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME).equals("")) {
                                                        txtAddtoCart.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME));
                                                    } else {
                                                        txtAddtoCart.setText(getString(R.string.notify_me));
                                                    }
                                                } else {
                                                    txtAddtoCart.setText(getString(R.string.notify_me));
                                                }
                                            } else {
                                                if (!strProductID.equalsIgnoreCase("")) {
                                                    CheckItemInCart(strProductID);
                                                } else {
                                                    CheckItemInCart(strMainProductID);
                                                }
                                            }
                                        }

                                        String strSlug = jsonObjectProducts.optString("slug");
                                        getReviews(strSlug);
                                    }

                                }

                                getViewPager(ViewPagerSlider);

                                RelatedProducts(slug);
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }


                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region ADAPTER AdapterAttributeTitle...
    public class AdapterAttributeTitle extends RecyclerView.Adapter<AdapterAttributeTitle.Viewholder> {

        Context context;
        JSONArray jsonArray;

        public AdapterAttributeTitle(Context context, JSONArray jsonArray) {
            this.context = context;
            this.jsonArray = jsonArray;
        }

        @Override
        public AdapterAttributeTitle.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_attribute_title, viewGroup, false);
            return new AdapterAttributeTitle.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, final int position) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(position);
                VariationObject = jsonArray.length();
                if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT) != null) {
                    if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT).equals("")) {
                        viewholder.txtTitleVariation.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT)
                                + " " + jsonObject.getString("name"));
                    } else {
                        viewholder.txtTitleVariation.setText(context.getString(R.string.select) + " " + jsonObject.getString("name"));
                    }
                } else {
                    viewholder.txtTitleVariation.setText(context.getString(R.string.select) + " " + jsonObject.getString("name"));
                }
                JSONArray jsonArray = jsonObject.getJSONArray("terms");
                AdapterAttribute adapterAttribute = new AdapterAttribute(getContext(),
                        jsonArray, position, jsonArray.length());
                viewholder.recyclerviewVariation.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                viewholder.recyclerviewVariation.setAdapter(adapterAttribute);

                if (position == VariationObject - 1) {
                    viewholder.viewVariation.setVisibility(View.GONE);
                }
                if (position != 0) {
                    count++;
                } else {
                    count = 0;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            TextView txtTitleVariation;
            RecyclerView recyclerviewVariation;
            View viewVariation;

            public Viewholder(View itemView) {
                super(itemView);
                txtTitleVariation = itemView.findViewById(R.id.txtTitleVariation);
                recyclerviewVariation = itemView.findViewById(R.id.recyclerviewVariation);
                viewVariation = (View) itemView.findViewById(R.id.viewVariation);
                txtTitleVariation.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtTitleVariation.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            }
        }

    }
    //endregion

    //region ADAPTER AdapterAttribute...
    public class AdapterAttribute extends RecyclerView.Adapter<AdapterAttribute.Viewholder> {
        Context context;
        JSONArray jsonArray;
        boolean isClick = false;
        int Position, arraySize;
        String attributetTerm = "";

        public AdapterAttribute(Context context, JSONArray jsonArray, int Position, int arraySize) {
            this.context = context;
            this.jsonArray = jsonArray;
            this.arraySize = arraySize;
            this.Position = Position;
            arrayListAttribute = new ArrayList<>();
            arrayListAttributeTerm = new ArrayList<>();
        }

        @Override
        public AdapterAttribute.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_variation, viewGroup, false);
            return new AdapterAttribute.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, final int position) {
            try {
                final JSONObject jsonObject = jsonArray.getJSONObject(position);
                viewholder.txtVariationName.setText(jsonObject.getString("attritem_name"));
                String name = jsonObject.getString("name");
                String attritem_name = jsonObject.getString("attritem_name");
                String productid = jsonObject.getString("z_productid_fk");
                String z_proattritemrelid_pk = jsonObject.getString("z_proattritemrelid_pk");
                String z_attributeid_pk = jsonObject.getString("z_attributeid_pk");
                String z_attributetermid_fk = jsonObject.getString("z_attributetermid_fk");

                attributetTerm = attributetTerm + "," + z_attributetermid_fk;
                strAttrTerm[Position] = attributetTerm;

                /*if (SharedPreference.GetPreference(getContext(), Global.IsClickPreference, StaticUtility.ArrayAttribut) != null) {
                    attrArray = new JSONArray(SharedPreference.GetPreference(getContext(), Global.IsClickPreference, StaticUtility.ArrayAttribut));
                    attrtermArray = new JSONArray(SharedPreference.GetPreference(getContext(), Global.IsClickPreference, StaticUtility.ArrayAttributTerm));
                    for (int i = 0; i < attrArray.length(); i++) {
                        arrayListAttribute.add(String.valueOf(attrArray.get(i)));
                        arrayListAttributeTerm.add(String.valueOf(attrtermArray.get(i)));
                    }
                }*/

                if (arrayListAttribute.size() > 0) {
                    if (arrayListAttribute.contains(z_attributeid_pk)) {
                        if (arrayListAttributeTerm.contains(z_attributetermid_fk)) {
                            chanageTextViewBorder(viewholder.txtVariationName);
                        } else {
                            chanageTextViewBorder1(viewholder.txtVariationName);
                        }
                    }
                } else {
                    chanageTextViewBorder1(viewholder.txtVariationName);
                    if (defaultVariations.size() > 0) {
                        for (int i = 0; i < defaultVariations.size(); i++) {
                            if (defaultVariations.get(i).getAttritem_name().contains(attritem_name)) {
                                chanageTextViewBorder(viewholder.txtVariationName);
                            }
                        }
                    }
                }


                viewholder.llVariation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            txtAddtoCart.setEnabled(true);
                            arrayListAttribute = new ArrayList<>();
                            arrayListAttributeTerm = new ArrayList<>();

                            JSONObject jsonObject1 = jsonArray.getJSONObject(position);
                            chanageTextViewBorder(viewholder.txtVariationName);
                            /*strArrayAttr[position] = jsonObject1.optString("z_attributeid_pk");*/
                            String Name = jsonObject.optString("attritem_name");
                            arrayListAttribute.add(jsonObject.optString("z_attributeid_pk"));
                            arrayListAttributeTerm.add(jsonObject.optString("z_attributetermid_fk"));
                            attrArray = new JSONArray();
                            attrtermArray = new JSONArray();
                            /*for (int j = 0; j < strAttrTerm.length; j++) {
                                if (strAttrTerm[j].contains(jsonObject.optString("z_attributetermid_fk"))) {
                                    strArrayAttrTerm[j] = jsonObject.optString("z_attributetermid_fk");
                                }
                            }*/
                            for (int j = 0; j < strAttrTerm.length; j++) {
                                if (strAttrTerm[j].contains(jsonObject.optString("z_attributetermid_fk"))) {
                                    strArrayAttrTerm[j] = jsonObject.optString("z_attributetermid_fk");
                                    strArrayAttr[j] = jsonObject.optString("z_attributeid_pk");
                                } else {
                                   /* for (int i = 0; i < defaultVariations.size(); i++) {
                                        if (!defaultVariations.get(i).getZ_attributetermid_fk().
                                                equalsIgnoreCase(jsonObject.optString("z_attributeid_pk"))) {
                                            strArrayAttrTerm[j] = defaultVariations.get(i).getZ_attributetermid_fk();
                                            strArrayAttr[j] = defaultVariations.get(i).getZ_attributeid_pk();
                                        }
                                    }*/
                                }
                            }
                            for (int i = 0; i < strArrayAttr.length; i++) {
                                attrArray.put(strArrayAttr[i]);
                                attrtermArray.put(strArrayAttrTerm[i]);
                            }
                            notifyDataSetChanged();
                            /*SharedPreference.CreatePreference(getContext(), Global.IsClickPreference);
                            SharedPreference.SavePreference(StaticUtility.ArrayAttribut, attrArray.toString());
                            SharedPreference.SavePreference(StaticUtility.ArrayAttributTerm, attrtermArray.toString());*/
                            getVariationSelection(String.valueOf(VariationObject));
//                            Toast.makeText(context, Name + " " + attrArray.toString() + attrtermArray.toString(), Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            TextView txtVariationName;
            LinearLayout llVariation;
            ImageView image_not_available;
            FrameLayout frameLayoutVariation;

            public Viewholder(View itemView) {
                super(itemView);
                txtVariationName = itemView.findViewById(R.id.txtVariationName);
                llVariation = itemView.findViewById(R.id.llVariation);
                image_not_available = itemView.findViewById(R.id.image_not_available);
                frameLayoutVariation = itemView.findViewById(R.id.frameLayoutVariation);

                txtVariationName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

                txtVariationName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            }
        }
    }
    //endregion

    //region FOR getVariationSelection API...
    private void getVariationSelection(String Count) {
        relativeProgress.setVisibility(View.VISIBLE);

        JSONObject jsonObjectParam = new JSONObject();

        try {
            jsonObjectParam.put("count", Count);
            jsonObjectParam.put("product_id", strMainProductID);
            jsonObjectParam.put("attrArr", attrArray);
            jsonObjectParam.put("attrtermArr", attrtermArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.DisableVariationOption);
        postRequestBuilder.addJSONObjectBody(jsonObjectParam)
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    txtAddtoWishlist.setVisibility(View.VISIBLE);
                                    JSONObject jsonObject = response.optJSONObject("payload");
                                    if (jsonObject.has("product_id")) {
                                        String product_id = jsonObject.optString("product_id");
                                        if (!product_id.equalsIgnoreCase("")) {
                                            getVariationData(product_id);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                /*Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();*/
                                if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sOUT_OF_STOCK) != null) {
                                    if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sOUT_OF_STOCK).equals("")) {
                                        txtAddtoCart.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sOUT_OF_STOCK));
                                    } else {
                                        txtAddtoCart.setText(getString(R.string.out_of_stock));
                                    }
                                } else {
                                    txtAddtoCart.setText(getString(R.string.out_of_stock));
                                }
                                txtAddtoWishlist.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR getVariationData API...
    private void getVariationData(String productid) {
        relativeProgress.setVisibility(View.VISIBLE);
        String strCurrencyCode = SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencyCode);
        String[] key = new String[]{"z_productid_pk", "currencyCode"};
        String[] val = new String[]{productid, strCurrencyCode};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetVariationData);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    strProductID = jsonObjectPayload.getString("product_id");
                                    strProductName = jsonObjectPayload.getString("name");
                                    strProductSKU = jsonObjectPayload.getString("sku");
                                    String slug = jsonObjectPayload.getString("slug");
                                    int Stock_Status = Integer.parseInt(jsonObjectPayload.getString("stock_status"));
                                    int quantity = Integer.parseInt(jsonObjectPayload.getString("quantity"));
                                    if (!jsonObjectPayload.getString("description").equals("")) {
                                        txtProductDetail.setText(jsonObjectPayload.getString("description"));
                                    } else {
                                        txtProductDetail.setText(strProductDes);
                                    }
                                    /*if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREAD_MORE) != null) {
                                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREAD_MORE).equals("")) {
                                            Global.MakeTextViewResizable(getContext(), txtProductDetail,
                                                    3, SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE,
                                                            StaticUtility.sREAD_MORE), true);
                                        } else {
                                            Global.MakeTextViewResizable(getContext(), txtProductDetail, 3, getString(R.string.read_more), true);
                                        }
                                    } else {
                                        Global.MakeTextViewResizable(getContext(), txtProductDetail, 3, getString(R.string.read_more), true);
                                    }*/

                                    if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.PRE_BOOKING_ENABLED) != null) {
                                        String strPreBookingEnable = SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.PRE_BOOKING_ENABLED);
                                        if (strPreBookingEnable.equalsIgnoreCase("1")) {
                                            String strCurrentDateTime = "", strPreBookingEndDate = "";
                                            if (jsonObjectPayload.has("pre_booking_enabled")) {
                                                String strPreBookingEnabled = jsonObjectPayload.optString("pre_booking_enabled");
                                                if (strPreBookingEnabled.equalsIgnoreCase("1")) {
                                                    strCurrentDateTime = jsonObjectPayload.optString("currentdatetime");
                                                    strPreBookingEndDate = jsonObjectPayload.optString("pre_booking_enddate");
                                                    setTimer(strCurrentDateTime, strPreBookingEndDate);
                                                    txtAddtoWishlist.setVisibility(View.GONE);
                                                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPRE_BOOKING) != null) {
                                                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPRE_BOOKING).equals("")) {
                                                            txtAddtoCart.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPRE_BOOKING));
                                                        } else {
                                                            txtAddtoCart.setText(getString(R.string.pre_booking));
                                                        }
                                                    } else {
                                                        txtAddtoCart.setText(getString(R.string.pre_booking));
                                                    }
                                                } else {
                                                    if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active).equals("1")) {
                                                        llCard.setVisibility(View.VISIBLE);
                                                        txtAddtoWishlist.setVisibility(View.VISIBLE);
                                                    } else {
                                                        txtAddtoWishlist.setVisibility(View.GONE);
                                                    }
                                                    if (Stock_Status == 0 || quantity <= 0) {
                                                        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME) != null) {
                                                            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME).equals("")) {
                                                                txtAddtoCart.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME));
                                                            } else {
                                                                txtAddtoCart.setText(getString(R.string.notify_me));
                                                            }
                                                        } else {
                                                            txtAddtoCart.setText(getString(R.string.notify_me));
                                                        }
                                                    } else {
                                                        if (!strProductID.equalsIgnoreCase("")) {
                                                            CheckItemInCart(strProductID);
                                                        } else {
                                                            CheckItemInCart(strMainProductID);
                                                        }
                                                    }
                                                }
                                            } else {
                                                if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active).equals("1")) {
                                                    llCard.setVisibility(View.VISIBLE);
                                                    txtAddtoWishlist.setVisibility(View.VISIBLE);
                                                } else {
                                                    txtAddtoWishlist.setVisibility(View.GONE);
                                                }
                                                if (Stock_Status == 0 || quantity <= 0) {
                                                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME) != null) {
                                                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME).equals("")) {
                                                            txtAddtoCart.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME));
                                                        } else {
                                                            txtAddtoCart.setText(getString(R.string.notify_me));
                                                        }
                                                    } else {
                                                        txtAddtoCart.setText(getString(R.string.notify_me));
                                                    }
                                                } else {
                                                    if (!strProductID.equalsIgnoreCase("")) {
                                                        CheckItemInCart(strProductID);
                                                    } else {
                                                        CheckItemInCart(strMainProductID);
                                                    }
                                                }
                                            }
                                        } else {
                                            if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active).equals("1")) {
                                                llCard.setVisibility(View.VISIBLE);
                                                txtAddtoWishlist.setVisibility(View.VISIBLE);
                                            } else {
                                                txtAddtoWishlist.setVisibility(View.GONE);
                                            }
                                            if (Stock_Status == 0 || quantity <= 0) {
                                                if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME) != null) {
                                                    if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME).equals("")) {
                                                        txtAddtoCart.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME));
                                                    } else {
                                                        txtAddtoCart.setText(getString(R.string.notify_me));
                                                    }
                                                } else {
                                                    txtAddtoCart.setText(getString(R.string.notify_me));
                                                }
                                            } else {
                                                if (!strProductID.equalsIgnoreCase("")) {
                                                    CheckItemInCart(strProductID);
                                                } else {
                                                    CheckItemInCart(strMainProductID);
                                                }
                                            }
                                        }
                                    } else {
                                        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active).equals("1")) {
                                            llCard.setVisibility(View.VISIBLE);
                                            txtAddtoWishlist.setVisibility(View.VISIBLE);
                                        } else {
                                            txtAddtoWishlist.setVisibility(View.GONE);
                                        }
                                        if (Stock_Status == 0 || quantity <= 0) {
                                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME) != null) {
                                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME).equals("")) {
                                                    txtAddtoCart.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME));
                                                } else {
                                                    txtAddtoCart.setText(getString(R.string.notify_me));
                                                }
                                            } else {
                                                txtAddtoCart.setText(getString(R.string.notify_me));
                                            }
                                        } else {
                                            if (!strProductID.equalsIgnoreCase("")) {
                                                CheckItemInCart(strProductID);
                                            } else {
                                                CheckItemInCart(strMainProductID);
                                            }
                                        }
                                    }

                                    //region gallery_images
                                    Object objProducts = jsonObjectPayload.get("gallery_images");
                                    sliders = new ArrayList<>();
                                    if (!objProducts.equals("")) {
                                        JSONArray jsonArrayGelleryImage = jsonObjectPayload.getJSONArray("gallery_images");
                                        JSONObject jsonObjectImages = jsonObjectPayload.getJSONObject("main_image");
                                        strImage = jsonObjectImages.getString("main_image");
                                        String images = jsonObjectImages.getString("main_image");
                                        img = new String[jsonArrayGelleryImage.length() + 1];
                                        img[0] = images;
                                        sliders.add(new Sliders(0, images));
                                        for (int i = 0; i < jsonArrayGelleryImage.length(); i++) {
                                            JSONObject jsonObjectGelleryImages = jsonArrayGelleryImage.getJSONObject(i);
                                            String GelleryImages = jsonObjectGelleryImages.getString("main_image");
                                            img[i + 1] = GelleryImages;
                                            sliders.add(new Sliders(i + 1, GelleryImages));
                                        }
                                    } else {
                                        img = new String[1];
                                        JSONObject jsonObjectImages = jsonObjectPayload.getJSONObject("main_image");
                                        String images = jsonObjectImages.getString("main_image");
                                        strImage = jsonObjectImages.getString("main_image");
                                        img[0] = images;
                                        sliders.add(new Sliders(0, images));
                                    }
                                    //endregion

                                    strBasePrice = jsonObjectPayload.getString("price");
                                    float floatSalePrice = Float.parseFloat(jsonObjectPayload.getString("price"));
                                    intBasePrice = Math.round(floatSalePrice);
                                    strSalePrice = jsonObjectPayload.getString("sale_price");
                                    float floatBasePrice = Float.parseFloat(strSalePrice);
                                    intSalePrice = Math.round(floatBasePrice);

                                    if (intSalePrice > 0) {
                                        if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                                                StaticUtility.sCurrencySignPosition).equals("1")) {
                                            txt_product_base_price.setVisibility(View.VISIBLE);
                                            textProductSalePrice.setText(SharedPreference.GetPreference(getContext(),
                                                    Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign) + " " + jsonObjectPayload.getString("sale_price"));
                                            txt_product_base_price.setText(SharedPreference.GetPreference(getContext(),
                                                    Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign) + " " + jsonObjectPayload.getString("price"));
                                        } else if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                                                StaticUtility.sCurrencySignPosition).equals("0")) {
                                            txt_product_base_price.setVisibility(View.VISIBLE);
                                            textProductSalePrice.setText(jsonObjectPayload.getString("sale_price") + " " +
                                                    SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign));
                                            txt_product_base_price.setText(jsonObjectPayload.getString("price") + " " +
                                                    SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign));
                                        }
                                    } else {
                                        if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                                                StaticUtility.sCurrencySignPosition).equals("1")) {
                                            textProductSalePrice.setText(SharedPreference.GetPreference(getContext(),
                                                    Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign) + " " + String.valueOf(intBasePrice));
                                            txt_product_base_price.setVisibility(View.GONE);
                                        } else if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                                                StaticUtility.sCurrencySignPosition).equals("0")) {
                                            textProductSalePrice.setText(String.valueOf(intBasePrice) + " " +
                                                    SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign));
                                            txt_product_base_price.setVisibility(View.GONE);
                                        }
                                    }
                                    getReviews(slug);
                                }
                                /*CheckItemInCart(strProductID);*/
                                getViewPager(ViewPagerSlider);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR CheckPincode API..
    private void CheckPincode() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"pincode"};
        String[] val = {editTextPincode.getText().toString()};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetCheckPincode);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    if (jsonObject.getString("count").equals("1")) {
                                        txtisAvalible.setVisibility(View.VISIBLE);
                                        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERY_AVAILABLE) != null) {
                                            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERY_AVAILABLE).equals("")) {
                                                txtisAvalible.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERY_AVAILABLE));
                                            } else {
                                                txtisAvalible.setText(getString(R.string.delivery_available));
                                            }
                                        } else {
                                            txtisAvalible.setText(getString(R.string.delivery_available));
                                        }
                                        /*strPincodeValidation = "Delivery Available";*/
                                        txtisAvalible.setTextColor(Color.parseColor("#008000"));
                                        imageCashondelivery.setImageResource(R.drawable.ic_right);
                                        SharedPreference.CreatePreference(getContext(), Global.LOGIN_PREFERENCE);
                                        SharedPreference.SavePreference(StaticUtility.sPincode, editTextPincode.getText().toString());
                                    } else {
                                        txtisAvalible.setVisibility(View.VISIBLE);
                                        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERY_NOT_AVAILABLE) != null) {
                                            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERY_NOT_AVAILABLE).equals("")) {
                                                txtisAvalible.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERY_NOT_AVAILABLE));
                                            } else {
                                                txtisAvalible.setText(getString(R.string.delivery_not_available));
                                            }
                                        } else {
                                            txtisAvalible.setText(getString(R.string.delivery_not_available));
                                        }
                                        txtisAvalible.setTextColor(Color.parseColor("#FF0000"));
                                        imageCashondelivery.setImageResource(R.drawable.ic_worrng);
                                        /*strPincodeValidation = "Delivery Not Available";*/
                                        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHECK_PINCODE_ERROR) != null) {
                                            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHECK_PINCODE_ERROR).equals("")) {
                                                strPincodeMessage = SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCHECK_PINCODE_ERROR);
                                            } else {
                                                strPincodeMessage = getString(R.string.please_enter_pincode_onther_pincode_because_delivery_not_available);
                                            }
                                        } else {
                                            strPincodeMessage = getString(R.string.please_enter_pincode_onther_pincode_because_delivery_not_available);
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR RelatedProducts API..
    private void RelatedProducts(String slug) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;

        String user_id = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        String strCurrencyCode = SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencyCode);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            key = new String[]{"slug", "user_id", "currencyCode"};
            val = new String[]{slug, user_id, strCurrencyCode};
        } else {
            key = new String[]{"slug", "user_id", "currencyCode"};
            val = new String[]{slug, "", strCurrencyCode};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.RelatedProduct);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {

                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    if (jsonObjectPayload.has("relatedproducts")) {
                                        jsonArrayRelatedProduct = jsonObjectPayload.getJSONArray("relatedproducts");
                                        if (jsonArrayRelatedProduct.length() > 0) {
                                            llRelatedProduct.setVisibility(View.VISIBLE);
                                            AdapterRelatedProduct adapterRelatedProduct = new AdapterRelatedProduct(getContext(), jsonArrayRelatedProduct);
                                            recyclerviewReletedProduct.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                                            recyclerviewReletedProduct.setAdapter(adapterRelatedProduct);
                                        } else {
                                            llRelatedProduct.setVisibility(View.GONE);
                                        }
                                    }
                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                llRelatedProduct.setVisibility(View.GONE);
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                /*Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();*/
                            }
                        } catch (JSONException | NullPointerException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region ADAPTER
    public class AdapterRelatedProduct extends RecyclerView.Adapter<AdapterRelatedProduct.Viewholder> {

        Context context;
        JSONArray jsonArray;

        public AdapterRelatedProduct(Context context, JSONArray jsonArray) {
            this.context = context;
            this.jsonArray = jsonArray;
        }

        @Override
        public AdapterRelatedProduct.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_product, viewGroup, false);
            return new AdapterRelatedProduct.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder viewholder, int position) {
            try {
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        viewholder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                    }
                }
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) viewholder.llProducts.getLayoutParams();
                if (position == 0) {
                    params.setMargins(15, 0, 5, 0);
                    viewholder.llProducts.setLayoutParams(params);
                } else if (position == getItemCount() - 1) {
                    params.setMargins(5, 0, 15, 0);
                    viewholder.llProducts.setLayoutParams(params);
                } else {
                    params.setMargins(5, 0, 5, 0);
                    viewholder.llProducts.setLayoutParams(params);
                }

                JSONObject jsonObject = jsonArray.getJSONObject(position);
                String strRating = jsonObject.getString("avg_rating");
                float floatSalePrice = Float.parseFloat(jsonObject.getString("price"));
                intBasePrice = Math.round(floatSalePrice);
                float floatBasePrice = Float.parseFloat(jsonObject.getString("sale_price"));
                intSalePrice = Math.round(floatBasePrice);
                final String product_id = jsonObject.getString("product_id");
                final String slug = jsonObject.getString("slug");
                String exists_in_wishlist = jsonObject.getString("exists_in_wishlist");
                final String productName = jsonObject.getString("name");
                viewholder.textProductName.setText(jsonObject.getString("name"));

                if (!strRating.equals("")) {
                    float rating = Float.parseFloat(strRating);
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                } else {
                    float rating = Float.parseFloat("0");
                    viewholder.reviewRatingbar.setVisibility(View.VISIBLE);
                    viewholder.reviewRatingbar.setRating(rating);
                }

                if (intSalePrice > 0) {
                    if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                            StaticUtility.sCurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(getContext(),
                                Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign) + " " + jsonObject.getString("sale_price"));
                        viewholder.txt_product_base_price.setText(SharedPreference.GetPreference(getContext(),
                                Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign) + " " + jsonObject.getString("price"));
                    } else if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                            StaticUtility.sCurrencySignPosition).equals("0")) {
                        viewholder.textProductSalePrice.setText(jsonObject.getString("sale_price") + " " +
                                SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign));
                        viewholder.txt_product_base_price.setText(jsonObject.getString("price") + " " +
                                SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign));
                    }
                } else {
                    if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                            StaticUtility.sCurrencySignPosition).equals("1")) {
                        viewholder.textProductSalePrice.setText(SharedPreference.GetPreference(getContext(),
                                Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign) + " " + jsonObject.getString("price"));
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    } else if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                            StaticUtility.sCurrencySignPosition).equals("0")) {
                        viewholder.textProductSalePrice.setText(jsonObject.getString("price") + " " +
                                SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign));
                        viewholder.txt_product_base_price.setVisibility(View.GONE);
                    }
                }

              /*  JSONArray jsonArrayImages = jsonObject.getJSONArray("main_image");
                String images = String.valueOf(jsonArrayImages.get(0));*/

                JSONObject jsonObjectImages = jsonObject.getJSONObject("main_image");
                String images = jsonObjectImages.getString("main_image");

                //region Image
                String picUrl = null;
                try {
                    URL urla = null;
                /*images = images.replace("[", "");
                image = image.replace("]", "");*/
                    urla = new URL(images.replaceAll("%20", " "));
                    URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    // Capture position and set to the ImageView
                    Picasso.get()
                            .load(picUrl)
                            .into(viewholder.imageProduct, new Callback() {
                                @Override
                                public void onSuccess() {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                    viewholder.rlImgHolder.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError(Exception e) {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment ViewPagerAdapter.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
                //endregion

                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.PRE_BOOKING_ENABLED) != null) {
                    String strPreBookingEnable = SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.PRE_BOOKING_ENABLED);
                    if (strPreBookingEnable.equalsIgnoreCase("1")) {
                        if (jsonObject.getString("pre_booking_enabled") != null) {
                            String strPreBookingEnabled = jsonObject.getString("pre_booking_enabled");
                            if (strPreBookingEnabled.equalsIgnoreCase("1")) {
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date startDate = null, endDate = null;

                                try {
                                    startDate = sdf.parse(jsonObject.getString("currentdatetime"));
                                    endDate = sdf.parse(jsonObject.getString("pre_booking_enddate"));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                Calendar start_calendar = Calendar.getInstance();
                                start_calendar.setTime(startDate);
                                Calendar end_calendar = Calendar.getInstance();
                                end_calendar.setTime(endDate);
                                long start_millis = start_calendar.getTimeInMillis(); //get the start time in milliseconds
                                long end_millis = end_calendar.getTimeInMillis(); //get the end time in milliseconds
                                long total_millis = (end_millis - start_millis); //total time in milliseconds

                                viewholder.countDownTimer = new CountDownTimer(total_millis, 1000) {
                                    @Override
                                    public void onTick(long millisUntilFinished) {

                                    }

                                    private String twoDigitString(long number) {
                                        if (number == 0) {
                                            return "00";
                                        } else if (number / 10 == 0) {
                                            return "0" + number;
                                        }
                                        return String.valueOf(number);
                                    }

                                    @Override
                                    public void onFinish() {
                                    }
                                };
                                viewholder.countDownTimer.start();
                                viewholder.imgWish.setVisibility(View.GONE);
                            } else {
                                if (is_wishlist.equals("1")) {
                                    viewholder.imgWish.setVisibility(View.VISIBLE);
                                } else {
                                    viewholder.imgWish.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            if (is_wishlist.equals("1")) {
                                viewholder.imgWish.setVisibility(View.VISIBLE);
                            } else {
                                viewholder.imgWish.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        if (is_wishlist.equals("1")) {
                            viewholder.imgWish.setVisibility(View.VISIBLE);
                        } else {
                            viewholder.imgWish.setVisibility(View.GONE);
                        }
                    }
                } else {
                    if (is_wishlist.equals("1")) {
                        viewholder.imgWish.setVisibility(View.VISIBLE);
                    } else {
                        viewholder.imgWish.setVisibility(View.GONE);
                    }
                }


                if (exists_in_wishlist.equals("0")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_heart);
                } else if (exists_in_wishlist.equals("1")) {
                    viewholder.imgWish.setImageResource(R.drawable.ic_select_heart);
                }

                viewholder.imgWish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AddToWishList(product_id, viewholder.imgWish);
                    }
                });

                viewholder.framelayoutProduct.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ProductDetailFragment productDetailFragment = new ProductDetailFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("slug", slug);
                        bundle.putString("name", productName);
                        fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        productDetailFragment.setArguments(bundle);
                        fragmentTransaction.replace(R.id.framLayoutProductDetail, productDetailFragment);
                        fragmentTransaction.commit();
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView imageProduct, imgWish;
            TextView textProductName, textProductSalePrice, txt_product_base_price;
            FrameLayout framelayoutProduct;
            RatingBar reviewRatingbar;
            LinearLayout llProducts;
            RelativeLayout rlImgHolder;
            ProgressBar pbImgHolder;
            CountDownTimer countDownTimer;

            public Viewholder(View itemView) {
                super(itemView);
                framelayoutProduct = itemView.findViewById(R.id.framelayoutProduct);
                imageProduct = itemView.findViewById(R.id.imageProduct);
                imgWish = itemView.findViewById(R.id.imgWish);
                textProductName = itemView.findViewById(R.id.textProductName);
                textProductSalePrice = itemView.findViewById(R.id.textProductSalePrice);
                txt_product_base_price = itemView.findViewById(R.id.txt_product_base_price);
                reviewRatingbar = itemView.findViewById(R.id.reviewRatingbar);
                llProducts = itemView.findViewById(R.id.llProducts);
                rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
                pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);

                LayerDrawable stars = (LayerDrawable) reviewRatingbar.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(1).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);

                textProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                textProductSalePrice.setTypeface(Typefaces.TypefaceCalibri_bold(context));
                txt_product_base_price.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

                textProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                textProductSalePrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txt_product_base_price.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));

            }
        }
    }
    //endregion

    //region FOR AddToWishList API..
    private void AddToWishList(final String product_id, final ImageView imageView) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"product_id"};
        String[] val = {product_id};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.AddToWishlist);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(getContext()));
        } else {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers2(getContext()));
        }
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                imageView.setImageResource(R.drawable.ic_select_heart);
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                mListener = (OnFragmentInteractionListener) getContext();
                                mListener.WishlistCount();
                                MainActivity.MostWantedListingRefresh(product_id, "1");
                                MainActivity.BestSellingListingRefresh(product_id, "1");
                                MainActivity.NewArrivalListingRefresh(product_id, "1");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");

                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else if (strMessage.equals("Product already exists in Wishlist!")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    imageView.setClickable(false);
                                } else {
                                    imageView.setClickable(true);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR AddToWishListProduct API..
    private void AddToWishListProduct(final String product_id) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"product_id"};
        String[] val = {product_id};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.AddToWishlist);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(getContext()));
        } else {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers2(getContext()));
        }
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                RelatedProducts(strSlug);
                                MainActivity.MostWantedListingRefresh(product_id, "1");
                                MainActivity.BestSellingListingRefresh(product_id, "1");
                                MainActivity.NewArrivalListingRefresh(product_id, "1");
                               /* mListener = (OnFragmentInteractionListener) getContext();
                                mListener.WishlistCount();*/
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else if (strMessage.equals("Product already exists in Wishlist!")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    txtAddtoWishlist.setClickable(false);
                                } else {
                                    txtAddtoWishlist.setClickable(true);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR getReviews API..
    private void getReviews(String slug) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"slug"};
        String[] val = {slug};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetReviews);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                reviewses = new ArrayList<>();
                                float floatRating = 0;
                                if (strStatus.equals("ok")) {
                                    Object object = response.get("payload");
                                    if (!object.equals("")) {
                                        JSONArray jsonArrayPayload = response.getJSONArray("payload");
                                        for (int i = 0; i < jsonArrayPayload.length(); i++) {
                                            JSONObject jsonObjectPayload = jsonArrayPayload.getJSONObject(i);
                                            String Username = jsonObjectPayload.getString("name");
                                            String description = jsonObjectPayload.getString("description");
                                            String Reviewdate = jsonObjectPayload.getString("added_on");
                                            String rating = jsonObjectPayload.getString("rating");
                                            float rating1 = Float.parseFloat(rating);
                                            floatRating = floatRating + rating1;
                                            reviewses.add(new Reviews(Username, Reviewdate, rating, description));
                                        }
                                        if (reviewses.size() > 0) {
                                            llReviews.setVisibility(View.VISIBLE);
                                            AdapterReviewsList adapterReviewsList = new AdapterReviewsList(getContext(), reviewses);
                                            recyclerviewReviews.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                            recyclerviewReviews.setAdapter(adapterReviewsList);
                                            floatRating = floatRating / reviewses.size();
                                            reviewRatingbar.setRating(floatRating);
                                        }
                                    } else {
                                        llReviews.setVisibility(View.GONE);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR Addtocart API...
    private void AddToCart(String ProductID) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;

        String userId = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        String SessionId = SharedPreference.GetPreference(getActivity(), Global.preferenceNameGuestUSer, Global.SessionId);
        String strCurrencyCode = SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencyCode);
        if (userId != null) {
            key = new String[]{"session_id", "user_id", "product_id", "product_name",
                    "category_id", "price", "currencyCode"};
            val = new String[]{"", userId, ProductID, txtProductName.getText().toString(), "",
                    strSalePrice, strCurrencyCode};
        } else {
            key = new String[]{"session_id", "user_id", "product_id", "currencyCode"};
            val = new String[]{SessionId, "", ProductID, txtProductName.getText().toString(),
                    "", strSalePrice, strCurrencyCode};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.AddToCart);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    productIDs.clear();
                                    variationAttribute.clear();
//                                    SharedPreference.ClearPreference(getContext(), Global.IsClickPreference);
                                    if (strMessage.equalsIgnoreCase("Product successfully added to cart!")) {
                                        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sGO_TO_CART) != null) {
                                            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sGO_TO_CART).equals("")) {
                                                txtAddtoCart.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sGO_TO_CART));
                                            } else {
                                                txtAddtoCart.setText(getString(R.string.go_to_cart));
                                            }
                                        } else {
                                            txtAddtoCart.setText(getString(R.string.go_to_cart));
                                        }
                                        mListener = (OnFragmentInteractionListener) getContext();
                                        mListener.onAddToCartProduct();
                                    } else if (strMessage.equalsIgnoreCase("Product already exists in cart!")) {
                                        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sGO_TO_CART) != null) {
                                            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sGO_TO_CART).equals("")) {
                                                txtAddtoCart.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sGO_TO_CART));
                                            } else {
                                                txtAddtoCart.setText(getString(R.string.go_to_cart));
                                            }
                                        } else {
                                            txtAddtoCart.setText(getString(R.string.go_to_cart));
                                        }
                                        txtAddtoCart.setClickable(true);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR Pre Booking API...
    private void PreBooking(String ProductID) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;

        String userId = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        String SessionId = SharedPreference.GetPreference(getActivity(), Global.preferenceNameGuestUSer, Global.SessionId);
        String strCurrencyCode = SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencyCode);
        if (userId != null) {
            key = new String[]{"session_id", "user_id", "product_id", "product_name",
                    "category_id", "price", "currencyCode", "is_prebooking"};
            val = new String[]{"", userId, ProductID, txtProductName.getText().toString(), "",
                    strSalePrice, strCurrencyCode, "1"};
        } else {
            key = new String[]{"session_id", "user_id", "product_id", "currencyCode", "is_prebooking"};
            val = new String[]{SessionId, "", ProductID, txtProductName.getText().toString(),
                    "", strSalePrice, strCurrencyCode, "1"};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.AddToCart);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    productIDs.clear();
                                    variationAttribute.clear();
                                    String strPreBookingid = response.optString("payload");
                                    SharedPreference.CreatePreference(getContext(), Global.CheckoutTotalPrice);
                                    SharedPreference.SavePreference(StaticUtility.strTotalPrice, strSalePrice);
                                    SharedPreference.SavePreference(StaticUtility.strTotalItems, "1");
                                    SharedPreference.SavePreference(StaticUtility.sPrebookingID, strPreBookingid);
                                    mListener = (OnFragmentInteractionListener) getContext();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("image", strImage);
                                    bundle.putString("name", strProductName);
                                    bundle.putString("sku", strProductSKU);
                                    bundle.putString("saleprice", strSalePrice);
                                    bundle.putString("baseprice", strBasePrice);
                                    bundle.putString("prebookingid", strPreBookingid);
                                    mListener.gotoCheckoutCart(bundle);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR CheckItemInCart API..
    private void CheckItemInCart(String strProductID) {
//        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;
        String userId = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);
        String SessionId = SharedPreference.GetPreference(getContext(), Global.preferenceNameGuestUSer, Global.SessionId);
        if (userId != null) {
            key = new String[]{"session_id", "user_id", "product_id"};
            val = new String[]{"", userId, strProductID};
        } else {
            key = new String[]{"session_id", "user_id", "product_id"};
            val = new String[]{SessionId, "", strProductID};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.CheckItemINCart);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
//                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                reviewses = new ArrayList<>();
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    int itemexists_count = Integer.parseInt(jsonObject.getString("itemexists_count"));
                                    if (itemexists_count > 0) {
                                        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sGO_TO_CART) != null) {
                                            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sGO_TO_CART).equals("")) {
                                                txtAddtoCart.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sGO_TO_CART));
                                            } else {
                                                txtAddtoCart.setText(getString(R.string.go_to_cart));
                                            }
                                        } else {
                                            txtAddtoCart.setText(getString(R.string.go_to_cart));
                                        }
                                    } else {
                                        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADD_TO_CART) != null) {
                                            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADD_TO_CART).equals("")) {
                                                txtAddtoCart.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADD_TO_CART));
                                            } else {
                                                txtAddtoCart.setText(getString(R.string.addtocart));
                                            }
                                        } else {
                                            txtAddtoCart.setText(getString(R.string.addtocart));
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
//                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR CheckItemInNofityMe API..
    private void CheckItemInNofityMe(final String ProductID) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"product_id"};
        String[] val = {ProductID};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.CheckNotifyMe);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(getContext()));
        } else {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers2(getContext()));
        }
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    String count = jsonObject.getString("count");
                                    if (Integer.parseInt(count) <= 0) {
                                        AddToNotifyMe(ProductID);
                                        txtAddtoCart.setEnabled(true);
                                    } else {
                                        Toast.makeText(getContext(), "User Already Notify This Product !", Toast.LENGTH_SHORT).show();
                                        txtAddtoCart.setEnabled(false);
                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    // region FOR AddToNotifyMe API..
    private void AddToNotifyMe(String ProductID) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"product_id"};
        String[] val = {ProductID};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.NotifyMe);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE,
                Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(getContext()));
        } else {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers2(getContext()));
        }
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    if (strStatus.equals("ok")) {
                                        Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region Keyboard Hide
    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    //endregion

    //region chanageTextViewBorder
    public void chanageTextViewBorder(TextView textView) {
        textView.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        textView.setBackgroundResource(R.drawable.ic_variation_background);
        GradientDrawable gd = (GradientDrawable) textView.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        gd.setCornerRadius(10);
    }
    //endregion

    //region chanageTextViewBorder1
    public void chanageTextViewBorder1(TextView textView) {
        textView.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        textView.setBackgroundResource(R.drawable.ic_variation_background);
        GradientDrawable gd = (GradientDrawable) textView.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(10);
//        gd.setStroke(1, Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
    }
    //endregion

    //region chanageEditTextBorder
    public void chanageEditTextBorder(EditText editText) {
        editText.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
    }
    //endregion

    //region chanageButton
    public void chanageButton(Button button) {
        button.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
//        button.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TEXTCOLOR)));
        button.setBackgroundResource(R.drawable.ic_button);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    //region FOR getDefualtVariationData API...
    private void getDefualtVariationData(String productid) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = new String[]{"product_id"};
        String[] val = new String[]{productid};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetDefualtVariationData);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    JSONArray jsonArrayDefualtVariation = jsonObjectPayload.getJSONArray("defaultVariationData");
                                    defaultVariations = new ArrayList<>();
                                    strArrayAttr = new String[jsonArrayDefualtVariation.length()];
                                    strArrayAttrTerm = new String[jsonArrayDefualtVariation.length()];
                                    for (int i = 0; i < jsonArrayDefualtVariation.length(); i++) {
                                        JSONObject jsonObjectDefaultVariation = jsonArrayDefualtVariation.getJSONObject(i);
                                        productVariationid = jsonObjectDefaultVariation.getString("z_productid_fk");
                                        Variationattritem_name = jsonObjectDefaultVariation.getString("attritem_name");
                                        String Proattritemrelid_pk = jsonObjectDefaultVariation.getString("z_proattritemrelid_pk");
                                        String Attributetermid_fk = jsonObjectDefaultVariation.getString("z_attributetermid_fk");
                                        String Attributeid_fk = jsonObjectDefaultVariation.getString("z_attributeid_fk");
                                        String Productattrrelid_fk = jsonObjectDefaultVariation.getString("z_productattrrelid_fk");
                                        String name = jsonObjectDefaultVariation.getString("name");
                                        String Attributeid_pk = jsonObjectDefaultVariation.getString("z_attributeid_pk");
                                        strArrayAttr[i] = jsonObjectDefaultVariation.optString("z_attributeid_fk");
                                        strArrayAttrTerm[i] = jsonObjectDefaultVariation.optString("z_attributetermid_fk");
                                        defaultVariations.add(new DefaultVariation(Proattritemrelid_pk,
                                                productVariationid, Attributetermid_fk, Attributeid_fk, Productattrrelid_fk,
                                                Variationattritem_name, name, Attributeid_pk));
                                    }
                                    strAttrTerm = new String[jsonArrayVariation.length()];
                                    AdapterAttributeTitle adapterAttributeTitle = new AdapterAttributeTitle(getContext(), jsonArrayVariation);
                                    recyclerviewVariation.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                    recyclerviewVariation.setAdapter(adapterAttributeTitle);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test Error", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region ShareDialog
    public void ShareDialog() {
        final CharSequence[] items;
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFACEBOOK) != null
                && SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTWITTER) != null
                && SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sGOOGLE) != null
                && SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSKYPE) != null
                && SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLINKDIN) != null
                && SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sWHATSAPP) != null
                && SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPRINTEREST) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFACEBOOK).equals("")
                    && !SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTWITTER).equals("")
                    && !SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sGOOGLE).equals("")
                    && !SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSKYPE).equals("")
                    && !SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLINKDIN).equals("")
                    && !SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sWHATSAPP).equals("")
                    && !SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPRINTEREST).equals("")) {
                items = new CharSequence[]{
                        SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFACEBOOK),
                        SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTWITTER),
                        SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sGOOGLE),
                        SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSKYPE),
                        SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLINKDIN),
                        SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sWHATSAPP),
                        SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPRINTEREST)};
            } else {
                items = new CharSequence[]{getString(R.string.facebook),
                        getString(R.string.twitter),
                        getString(R.string.google),
                        getString(R.string.skype),
                        getString(R.string.linkdin),
                        getString(R.string.printerest)};
            }
        } else {
            items = new CharSequence[]{getString(R.string.facebook),
                    getString(R.string.twitter),
                    getString(R.string.google),
                    getString(R.string.skype),
                    getString(R.string.linkdin),
                    getString(R.string.printerest)};
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_SHARE_TYPE) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_SHARE_TYPE).equals("")) {
                builder.setTitle(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_SHARE_TYPE));
            } else {
                builder.setTitle(getString(R.string.select_share_type));
            }
        } else {
            builder.setTitle(getString(R.string.select_share_type));
        }
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    dialog.dismiss();
                    ShareWithFacebook();
                } else if (item == 1) {
                    dialog.dismiss();
                    ShareWithTwitter();
                } else if (item == 2) {
                    dialog.dismiss();
                    ShareWithGoogle();
                } else if (item == 3) {
                    dialog.dismiss();
                    ShareWithSkype();
                } else if (item == 4) {
                    dialog.dismiss();
                    ShareWithLinkdin();
                } else if (item == 5) {
                    dialog.dismiss();
                    ShareWithPinterest();
                }
            }
        });
        builder.show();
    }//endregion

    //region Share With Facebook
    private void ShareWithFacebook() {
        String strURL = strWebSiteURL + "/product/" + strSlug;
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse(strURL))
                    .setContentTitle(strProductName)
                    .setContentDescription(strShortDescription)
                    .build();
            shareDialog.show(linkContent);
        }
    }//endregion

    //region Share With Twitter
    private void ShareWithTwitter() {
        String strURL = "http://" + strWebSiteURL + "/product/" + strSlug;
        String tweetUrl = "https://twitter.com/intent/tweet?url="
                + strURL;
        Uri uri = Uri.parse(tweetUrl);
        startActivity(new Intent(Intent.ACTION_VIEW, uri));

    }//endregion

    //region Share With Google
    private void ShareWithGoogle() {
        String strGoogleURL = "https://developers.google.com/+/";
        String strURL = strWebSiteURL + "/product/" + strSlug;
        /*PlusShare.Builder builder = new PlusShare.Builder(getActivity());*/
        Intent shareIntent = new PlusShare.Builder(getActivity())
                .setType("text/plain")
                .setText(strURL)
                .setContentUrl(Uri.parse(strGoogleURL))
                .getIntent();

        startActivityForResult(shareIntent, 0);
    }//endregion

    //region Share With Linkdin
    public void ShareWithLinkdin() {
        String url = "https://www.linkedin.com/sharing/share-offsite/?url="+strWebSiteURL + "/product/" + strSlug;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
        //LINK : https://developer.linkedin.com/docs/share-on-linkedin
        /*final String url = "https://api.linkedin.com/v1/people/~/shares";
        String strURL = strWebSiteURL + "/product/" + strSlug;
        JSONObject body = null;
        try {

            body = new JSONObject();
            JSONObject jsonObjectVisibility = new JSONObject();
            jsonObjectVisibility.put("code", "anyone");
            body.put("visibility", jsonObjectVisibility);
            JSONObject jsonObjectContent = new JSONObject();
            jsonObjectContent.put("title", strProductName);
            jsonObjectContent.put("description", strShortDescription);
            jsonObjectContent.put("submitted-url", strURL);
            body.put("content", jsonObjectContent);
        } catch (JSONException e) {
            e.printStackTrace();

        }
        final JSONObject finalBody = body;
        LISessionManager.getInstance(getApplicationContext()).init(getActivity(),
                Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS, Scope.W_SHARE)
                , new AuthListener() {
                    @Override
                    public void onAuthSuccess() {
                        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
                        apiHelper.postRequest(getContext(), url, finalBody, new ApiListener() {
                            @Override
                            public void onApiSuccess(ApiResponse apiResponse) {
                                // Success!
                                *//* Log.d(TAG, "Share response : " + apiResponse.toString());*//*
                                Toast.makeText(getContext(), "Shared successfully.", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onApiError(LIApiError liApiError) {
                                // Error making POST request!
                                *//*Log.e(TAG, "Share error : " + liApiError.getLocalizedMessage());*//*
                                Toast.makeText(getContext(), "Failed to share. Please try again.", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    @Override
                    public void onAuthError(LIAuthError error) {
                        Toast.makeText(getContext(), String.valueOf(error), Toast.LENGTH_SHORT).show();
                    }
                }, true);
*/

    }//endregion

    //region Share With Whatsapp
    private void ShareWithWhatsapp() {

        PackageManager pm = getActivity().getPackageManager();
        try {

            if (!isAPPClientInstalled(getContext(), "com.whatsapp")) {
                goToPlayStore(getContext(), "com.whatsapp");
                return;
            }

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");

            getActivity().getPackageManager().getPackageInfo("com.whatsapp",
                    PackageManager.GET_META_DATA);
            waIntent.setPackage("com.whatsapp");
            String strURL = strWebSiteURL + "/product/" + strSlug;
            waIntent.putExtra(Intent.EXTRA_TEXT, strURL);
            startActivity(Intent
                    .createChooser(waIntent, "Share"));

        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(getContext(), "Whatsapp have not been installed.",
                    Toast.LENGTH_SHORT).show();
        }
    }
    //endregion

    // region Share With Skype
    private void ShareWithSkype() {
        try {
            if (!isAPPClientInstalled(getContext(), "com.skype.raider")) {
                goToPlayStore(getContext(), "com.skype.raider");
                return;
            }

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            waIntent.setComponent(new ComponentName("com.skype.raider", "com.skype.raider.Main"));
            getActivity().getPackageManager().getPackageInfo("com.skype.raider",
                    PackageManager.GET_META_DATA);
            waIntent.setPackage("com.skype.raider");
            String strURL = strWebSiteURL + "/product/" + strSlug;
            waIntent.putExtra(Intent.EXTRA_TEXT, strURL);
            startActivity(Intent
                    .createChooser(waIntent, "Share"));

        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(getContext(), "Skype have not been installed.",
                    Toast.LENGTH_SHORT).show();
        }


       /*

        String strURL = "http://" + strWebSiteURL + "/product/" + strSlug;
        Uri skypeUri = Uri.parse(strURL);
        *//*Intent myIntent = new Intent(Intent.ACTION_SEND);
        myIntent.putExtra(Intent.EXTRA_TEXT, skypeUri);
        myIntent.setComponent(new ComponentName("com.skype.raider", "com.skype.raider.Main"));
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(myIntent);*//*
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.setComponent(new ComponentName("com.skype.raider", "com.skype.raider.Main"));
        shareIntent.putExtra(Intent.EXTRA_TEXT, skypeUri);
        startActivity(shareIntent);*/

    }//endregion

    //region Playstore redirection
    public boolean isAPPClientInstalled(Context myContext, String strAPP) {
        PackageManager myPackageMgr = myContext.getPackageManager();
        try {
            myPackageMgr.getPackageInfo(strAPP, PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            return (false);
        }
        return (true);
    }

    public void goToPlayStore(Context myContext, String strAPP) {
        Uri marketUri = Uri.parse("market://details?id=" + strAPP);
        Intent myIntent = new Intent(Intent.ACTION_VIEW, marketUri);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        myContext.startActivity(myIntent);

        return;
    }//endregion

    //region share with Pinterest
    private void ShareWithPinterest() {
        /*Uri lPictureUri = null;
         *//*lPictureUri = Uri.fromFile(new File("mnt/sdcard/1.jpg"));*//*
        String strURL = strWebSiteURL + "/product/" + strSlug;
        lPictureUri = Uri.parse(strURL);
        lIntentBuilder.setStream(lPictureUri);
        String lType = null != lPictureUri ? "image/jpeg" : "text/plain";
        lIntentBuilder.setType(lType);
        lIntentBuilder.setText("My Description");
        Intent shareIntent = lIntentBuilder.getIntent().setPackage("com.pinterest");
        shareIntent.putExtra("com.pinterest.EXTRA_DESCRIPTION", "Messagessssssssss");
        startActivity(shareIntent);*/

        try {
            if (!isAPPClientInstalled(getContext(), "com.pinterest")) {
                goToPlayStore(getContext(), "com.pinterest");
                return;
            }

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            getActivity().getPackageManager().getPackageInfo("com.pinterest",
                    PackageManager.GET_META_DATA);
            waIntent.setPackage("com.pinterest");
            String strURL = strWebSiteURL + "/product/" + strSlug;
            waIntent.putExtra(Intent.EXTRA_TEXT, strURL);
            startActivity(Intent
                    .createChooser(waIntent, "Share"));

        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(getContext(), "Pinterest have not been installed.",
                    Toast.LENGTH_SHORT).show();
        }
    }
    //endregion

    //region setTimer
    public void setTimer(String strStartDate, String strEndDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = null, endDate = null;

        try {
            startDate = sdf.parse(strStartDate);
            endDate = sdf.parse(strEndDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar start_calendar = Calendar.getInstance();
        start_calendar.setTime(startDate);
        Calendar end_calendar = Calendar.getInstance();
        end_calendar.setTime(endDate);
        long start_millis = start_calendar.getTimeInMillis(); //get the start time in milliseconds
        long end_millis = end_calendar.getTimeInMillis(); //get the end time in milliseconds
        long total_millis = (end_millis - start_millis); //total time in milliseconds

        CountDownTimer cdt = new CountDownTimer(total_millis, 1000) {
            @SuppressLint("SetTextI18n")
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.Is_Wishlist_Active).equals("1")) {
                    llCard.setVisibility(View.VISIBLE);
                    txtAddtoWishlist.setVisibility(View.VISIBLE);
                } else {
                    txtAddtoWishlist.setVisibility(View.GONE);
                }
                if (Stock_Status == 0 || quantity <= 0) {
                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME) != null) {
                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME).equals("")) {
                            txtAddtoCart.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNOTIFY_ME));
                        } else {
                            txtAddtoCart.setText(getString(R.string.notify_me));
                        }
                    } else {
                        txtAddtoCart.setText(getString(R.string.notify_me));
                    }
                } else {
                    if (!strProductID.equalsIgnoreCase("")) {
                        CheckItemInCart(strProductID);
                    } else {
                        CheckItemInCart(strMainProductID);
                    }
                }
            }
        };
        cdt.start();
    }//endregion

    //region FOR FullViewImage Dialog...
    private void FullViewImageDialog(int position) {
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View inflatedView = layoutInflater.inflate(R.layout.row_layout_full_view_image, null, false);

        //region widgetInitialization
        ImageView mImgCancel = inflatedView.findViewById(R.id.imgCancel);
        ExtendedViewPager mVpFullImageview = inflatedView.findViewById(R.id.vpFullImageview);
        //endregion

        //region widgetOnClick
        mImgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFullViewImageDialog.dismiss();
            }
        });
        //endregion

        mFullViewImageDialog = new Dialog(getContext(), R.style.CustomizeDialogTheme);
        mFullViewImageDialog.setContentView(inflatedView);
        FullViewImageAdapter fullViewImageAdapter = new FullViewImageAdapter(getContext(), sliders);
        mVpFullImageview.setAdapter(fullViewImageAdapter);
        mVpFullImageview.setCurrentItem(position);

        if (mFullViewImageDialog.isShowing()) {
            mFullViewImageDialog.dismiss();
        } else {
            mFullViewImageDialog.show();
        }
    }
    //endregion
}
